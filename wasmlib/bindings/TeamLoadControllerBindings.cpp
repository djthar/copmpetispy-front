#include <emscripten.h>
#include <emscripten/bind.h>

#include "SwimmerTimesController.h"
#include "TeamLoadController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(TeamLoadControllerBindings) {
  using namespace emscripten;

  class_<TeamLoadController>("TeamLoadController")
      .function("getSwimmerTimesControllerForNewSwimmer",
                select_overload<SwimmerTimesController(const Swimmer::SwimmerId&)>(
                    &TeamLoadController::getSwimmerTimesControllerForNewSwimmer));
}
