#include <emscripten.h>
#include <emscripten/bind.h>

#include "SwimmerToSessionController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SwimmerToSessionControllerBindings) {
  using namespace emscripten;

  class_<SwimmerToSessionController>("SwimmerToSessionController")
      .function("addSwimmerToSession", &SwimmerToSessionController::addSwimmerToSession)
      .function("removeSwimmerFromSession", &SwimmerToSessionController::removeSwimmerFromSession)
      .function("toggleSwimmerFromSession", &SwimmerToSessionController::toggleSwimmerFromSession);
}
