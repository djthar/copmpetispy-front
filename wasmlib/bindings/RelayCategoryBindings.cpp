#include <emscripten.h>
#include <emscripten/bind.h>

#include "RelayCategory.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(RelayCategoryBindings) {
  using namespace emscripten;

  /*
  using RelayCategoryRange = std::tuple<uint16_t, uint16_t>;

  class_<RelayCategoryRange>("RelayCategoryRange")
      .property("low", [](RelayCategoryRange& categoryRange) { return std::get<0>(categoryRange); })

      .property("high",
                [](RelayCategoryRange& categoryRange) { return std::get<1>(categoryRange); });

  function("getRelayCategoryRange", &getRelayCategoryRange);
*/

  enum_<RelayCategory>("RelayCategory")
      .value("_80", RelayCategory::_80)
      .value("_100", RelayCategory::_100)
      .value("_120", RelayCategory::_120)
      .value("_160", RelayCategory::_160)
      .value("_200", RelayCategory::_200)
      .value("_240", RelayCategory::_240)
      .value("_280", RelayCategory::_280)
      .value("_320", RelayCategory::_320)
      .value("_360", RelayCategory::_360);
}
