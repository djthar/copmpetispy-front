#include <emscripten.h>
#include <emscripten/bind.h>

#include "Swimmer.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SwimmerBindings) {
  using namespace emscripten;

  class_<Swimmer::SwimmerId>("SwimmerId")
      .constructor<std::string, std::string, uint16_t, Gender>()
      .property("name", &Swimmer::SwimmerId::name)
      .property("surname", &Swimmer::SwimmerId::surname)
      .property("bornYear", &Swimmer::SwimmerId::bornYear)
      .property("gender", &Swimmer::SwimmerId::gender);

  /*
  class_<Swimmer>("Swimmer")
      //.constructor<std::string, std::string, uint16_t, Gender>()
      .function("getId", &Swimmer::getId)
      .function("getName", &Swimmer::getName)
      .function("getSurname", &Swimmer::getSurname)
      .function("getBornYear", &Swimmer::getBornYear)
      .function("getGender", &Swimmer::getGender)
      .function("getAge", &Swimmer::getAge)
      .function("getTime", &Swimmer::getTime)
      .function("setEventTime", &Swimmer::setEventTime);
  */
}
