#include <emscripten.h>
#include <emscripten/bind.h>

#include "RelaySummaryController.h"
#include "SessionSummaryController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SessionSummaryControllerBinding) {
  using namespace emscripten;

  register_vector<RelaySummaryController::RelaySummaryEntry>(
      "VectorRelaySummaryController_RelaySummaryEntry");

  class_<SessionSummaryController>("SessionSummaryController")
      .function("getSummary", &SessionSummaryController::getSummary);
}
