#include <emscripten.h>
#include <emscripten/bind.h>

#include "Relay.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(RelayBindings) {
  using namespace emscripten;

  value_object<Relay::Id>("RelayId")
      .field("event", &Relay::Id::event)
      .field("category", &Relay::Id::category)
      .field("pool", &Relay::Id::pool);

  /*
class_<Relay>("Relay")
.function("getSumAge", &Relay::getSumAge)
.function("getPositionData", &Relay::getPositionData)
.function("getLeftPositions", &Relay::getLeftPositions)
.function("getNewSwimmerAgeRange", &Relay::getNewSwimmerAgeRange)
.function("getGenderCount", &Relay::getGenderCount)
.function("needsSwimmerFromGender", &Relay::needsSwimmerFromGender)
.function("getAccumulatedTime", &Relay::getAccumulatedTime)
.function("isFull", &Relay::isFull)
.function("getSnapshot", &Relay::getSnapshot)
.function("getSwimmers", &getEventSnapshot)
.function("getHeader", select_overload<std::string(const Relay&)>(&getRelayHeader));
*/
}
