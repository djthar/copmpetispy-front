#include <emscripten.h>
#include <emscripten/bind.h>

#include "SessionOperationsController.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(SessionOperationsControllerBindings) {
  using namespace emscripten;

  class_<SessionOperationsController>("SessionOperationsController")
      .function("addSwimmer", select_overload<void(const Swimmer::SwimmerId&)>(
                                  &SessionOperationsController::addSwimmer))
      .function("removeSwimmer", select_overload<void(const Swimmer::SwimmerId&)>(
                                     &SessionOperationsController::removeSwimmer))
      .function("hasSwimmer", select_overload<bool(const Swimmer::SwimmerId&) const>(
                                  &SessionOperationsController::hasSwimmer))
      .function("isSwimmerAvailable", select_overload<bool(const Swimmer::SwimmerId&) const>(
                                          &SessionOperationsController::isSwimmerAvailable))
      .function("addRelay",
                select_overload<void(const Relay::Id&)>(&SessionOperationsController::addRelay))
      .function("deleteRelay",
                select_overload<void(const Relay::Id&)>(&SessionOperationsController::deleteRelay))
      .function("hasRelay", select_overload<bool(const Relay::Id&) const>(
                                &SessionOperationsController::hasRelay))
      .function("getRelaySummaryController",
                &SessionOperationsController::getRelaySummaryController);
}
