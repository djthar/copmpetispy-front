#include <emscripten.h>
#include <emscripten/bind.h>

#include "Times.h"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(TimesBindings) {
  using namespace emscripten;

  enum_<Times::Pool>("TimesPool").value("_25", Times::Pool::_25).value("_50", Times::Pool::_50);

  class_<Times>("Times")
      .constructor()
      .function("getTime", &Times::getTime)
      .function("getTimeAutoConvert", &Times::getTimeAutoConvert)
      .function("setEventTime", &Times::setEventTime);
}
