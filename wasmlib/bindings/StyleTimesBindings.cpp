#include <emscripten.h>

#include "StyleTimes.h"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(StyleTimesBindings) {
  using namespace emscripten;

  class_<StyleTimes>("StyleTimes")
      .constructor<Style>()
      .function("getTimeForDistance", &StyleTimes::getTimeForDistance)
      .function("setTimeForDistance", &StyleTimes::setTimeForDistance)
      .function("getStyle", &StyleTimes::getStyle)
      .function("isDistanceValid", &StyleTimes::isDistanceValid)
      .class_function("timesEqual", StyleTimes::timesEqual)
      .class_function("isNoTime", StyleTimes::isNoTime);
}
