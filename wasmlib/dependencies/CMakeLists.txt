if(DEFINED ADD_TESTS)
    set(BUILD_TESTING OFF)
    add_subdirectory(googletest EXCLUDE_FROM_ALL)
endif()

set(JSON_BuildTests OFF CACHE INTERNAL "")
add_subdirectory(json EXCLUDE_FROM_ALL)
add_subdirectory(spdlog EXCLUDE_FROM_ALL)
