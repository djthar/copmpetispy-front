#ifndef COMPETYSPY_FRONT_SWIMMER_PROXY_PROVIDER_H
#define COMPETYSPY_FRONT_SWIMMER_PROXY_PROVIDER_H

#include <stdexcept>
#include <unordered_map>

#include "SwimmerLocal.h"
#include "SwimmerProvider.h"
#include "SwimmerProxy.h"

class SwimmerProxyProvider : public SwimmerProvider {
  using SwimmersCollection = std::unordered_map<Swimmer::SwimmerId, std::unique_ptr<SwimmerLocal>,
                                                Swimmer::SwimmerId::Hash>;

  class SwimmersCollectionIterator : public ConstSwimmerIterator {
  public:
    explicit SwimmersCollectionIterator(const SwimmersCollection& collection) :
        it_(collection.begin()),
        end_(collection.end()) {}

    auto getNext() -> const Swimmer& override {
      if (hasMore()) { ++it_; }
      return *(it_->second);
    }

    [[nodiscard]] auto hasMore() const -> bool override { return it_ != end_; }

  private:
    SwimmersCollection::const_iterator       it_;
    const SwimmersCollection::const_iterator end_;
  };

public:
  auto getSwimmerPtr(const Swimmer::SwimmerId& id) -> std::unique_ptr<Swimmer> {
    if (const auto& it = swimmers_.find(id); it != swimmers_.end()) {
      return std::make_unique<SwimmerProxy>(*it->second);
    } else {
      return nullptr;
    }
  }

  auto getSwimmerPtr(const std::string& id) -> std::unique_ptr<Swimmer> {
    if (const auto& it = swimmerIds.find(id); it != swimmerIds.end()) {
      return std::make_unique<SwimmerProxy>(*swimmers_.at(it->second));
    } else {
      return nullptr;
    }
  }

  auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override {
    if (const auto& it = swimmers_.find(id); it != swimmers_.end()) {
      return *it->second;
    } else {
      throw std::logic_error("Swimmer provider doesn't contain swimmer");
    }
  }

  auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override {
    if (const auto& it = swimmers_.find(id); it != swimmers_.end()) {
      return *it->second;
    } else {
      throw std::logic_error("Swimmer provider doesn't contain swimmer");
    }
  }

  auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override {
    return std::make_unique<SwimmersCollectionIterator>(swimmers_);
  }

  auto getSwimmer(const std::string& id) const -> const Swimmer& {
    if (const auto& it = swimmerIds.find(id); it != swimmerIds.end()) {
      return *swimmers_.at(it->second);
    } else {
      throw std::logic_error("Swimmer provider doesn't contain swimmer");
    }
  }

  auto getSwimmerId(const std::string& id) const -> const Swimmer::SwimmerId& {
    if (const auto& it = swimmerIds.find(id); it != swimmerIds.end()) {
      return swimmers_.at(it->second)->getId();
    } else {
      throw std::logic_error("Swimmer provider doesn't contain swimmer");
    }
  }

  void add(const std::string& simpleId, SwimmerLocal swimmer) {
    swimmerIds.emplace(simpleId, swimmer.getId());
    Swimmer::SwimmerId id = swimmer.getId();
    swimmers_.emplace(std::move(id), std::make_unique<SwimmerLocal>(std::move(swimmer)));
  }

private:
  std::unordered_map<std::string, Swimmer::SwimmerId> swimmerIds;
  SwimmersCollection                                  swimmers_;
};

#endif
