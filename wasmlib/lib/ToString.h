#ifndef COMPETYSPY_FRONT_TOSTRING_H
#define COMPETYSPY_FRONT_TOSTRING_H

#include <string>

#include "Event.h"
#include "Gender.h"
#include "Relay.h"
#include "RelayCategory.h"
#include "Style.h"

namespace ToString {

template<typename T>
std::string to_string(const T& value);

template<>
inline std::string to_string(const Style& style) {
  switch (style) {
    case Style::BUTTERFLY :
      return "mariposa";
    case Style::BACK :
      return "espalda";
    case Style::BREAST :
      return "braza";
    case Style::FREE :
      return "libre";
    case Style::MEDLEY :
      return "estilos";
  }
}

template<>
inline std::string to_string(const Gender& gender) {
  switch (gender) {
    case Gender::MALE :
      return "masculino";
    case Gender::FEMALE :
      return "femenino";
  }
}

template<>
inline std::string to_string(const EventGender& gender) {
  switch (gender) {
    case EventGender::MALE :
      return "masculino";
    case EventGender::FEMALE :
      return "femenino";
    case EventGender::MIXED :
      return "mixto";
  }
}

template<>
inline std::string to_string(const RelayCategory& category) {
  switch (category) {
    case RelayCategory::_80 :
      return "80";
    case RelayCategory::_100 :
      return "100";
    case RelayCategory::_120 :
      return "120";
    case RelayCategory::_160 :
      return "160";
    case RelayCategory::_200 :
      return "200";
    case RelayCategory::_240 :
      return "240";
    case RelayCategory::_280 :
      return "280";
    case RelayCategory::_320 :
      return "320";
    case RelayCategory::_360 :
      return "360";
  }
}

template<>
inline std::string to_string(const Event& event) {
  return std::to_string(event.distance) + " " + to_string(event.style) + " "
       + to_string(event.gender);
}

template<>
inline std::string to_string(const Relay& relay) {
  return "4x" + to_string(relay.getEvent()) + " +" + to_string(relay.getCategory());
}

}  // namespace ToString

#endif
