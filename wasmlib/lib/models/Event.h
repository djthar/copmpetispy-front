#ifndef COMPETYSPY_FRONT_EVENT_H
#define COMPETYSPY_FRONT_EVENT_H

#include <cstdint>
#include <nlohmann/json.hpp>

#include "Gender.h"
#include "Style.h"
#include "hash_combine.h"

enum class EventGender {
  MALE,
  FEMALE,
  MIXED
};

NLOHMANN_JSON_SERIALIZE_ENUM(EventGender, {
                                            {  EventGender::MALE,   "male"},
                                            {EventGender::FEMALE, "female"},
                                            { EventGender::MIXED,  "mixed"},
})

inline auto operator==(const Gender& l, const EventGender& r) -> bool {
  return (l == Gender::MALE && r == EventGender::MALE)
      || (l == Gender::FEMALE && r == EventGender::FEMALE) || r == EventGender::MIXED;
}

struct Event {
  Style       style                                                    = Style::FREE;
  uint16_t    distance                                                 = 50;
  EventGender gender                                                   = EventGender::MIXED;

  friend auto operator<=>(const Event& lhs, const Event& rhs) noexcept = default;

  auto        operator==(Event const& rhs) const -> bool {
    return style == rhs.style && distance == rhs.distance && gender == rhs.gender;
  }

  struct Hash {
    auto operator()(const Event& relay) const -> size_t {
      std::size_t h = 0;
      hash_combine(h, relay.style, relay.gender, relay.distance);
      return h;
    }
  };
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(Event, style, distance, gender);

#endif  // COMPETYSPY_FRONT_EVENT_H
