#ifndef COMPETYSPY_FRONT_STYLE_H
#define COMPETYSPY_FRONT_STYLE_H

#include <nlohmann/json.hpp>

#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

enum class Style {
  BUTTERFLY,
  BACK,
  BREAST,
  FREE,
  MEDLEY
};

NLOHMANN_JSON_SERIALIZE_ENUM(Style, {
                                      {Style::BUTTERFLY, "butterfly"},
                                      {     Style::BACK,      "back"},
                                      {   Style::BREAST,    "breast"},
                                      {     Style::FREE,      "free"},
                                      {   Style::MEDLEY,    "medley"},
})

inline auto operator<<(std::ostream& os, const Style& s) -> std::ostream& {
  os << "[Style] style=";
  switch (s) {
    case Style::BUTTERFLY :
      os << "butterfly";
    case Style::BACK :
      os << "back";
    case Style::BREAST :
      os << "breast";
    case Style::FREE :
      os << "free";
    case Style::MEDLEY :
      os << "medley";
  }
  return os;
}

#endif  // COMPETYSPY_FRONT_STYLE_H
