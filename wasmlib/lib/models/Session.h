#ifndef COMPETYSPY_FRONT_SESSION_H
#define COMPETYSPY_FRONT_SESSION_H

#include <functional>
#include <memory>
#include <unordered_map>

#include "spdlog/spdlog.h"

#include "Event.h"
#include "Relay.h"
#include "Swimmer.h"
#include "SwimmerEventCollection.h"
#include "SwimmerProvider.h"
#include "SwimmerProxy.h"
#include "Times.h"

#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

class Session : public SwimmerProvider {
  using SwimmersNotifyCollection
      = std::unordered_map<::Swimmer::SwimmerId, std::function<void()>, ::Swimmer::SwimmerId::Hash>;

public:
  class Swimmer : public SwimmerProxy {
  public:
    Swimmer(const ::Swimmer& swimmer, Session& session);
    ~Swimmer() override;

  private:
    Session& session_;
  };

  class ConstRelayIterator {
  public:
    virtual ~ConstRelayIterator()                        = default;
    virtual auto               getNext() -> const Relay& = 0;
    [[nodiscard]] virtual auto hasMore() const -> bool   = 0;
  };

  ~Session() override = default;

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& swimmerId) const
      -> const ::Swimmer& override
      = 0;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& swimmerId) -> ::Swimmer& override = 0;
  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override
      = 0;

  virtual void addSwimmer(const ::Swimmer::SwimmerId& swimmerId) = 0;

  void         removeSwimmer(const ::Swimmer::SwimmerId& swimmerId) {
    if (const auto& notifiersIt = swimmerRemoveNotifiers_.find(swimmerId);
        notifiersIt != swimmerRemoveNotifiers_.end()) {
      spdlog::debug("Calling destroy notifier for swimmer {}", swimmerId);
      notifiersIt->second();
    }
    _removeSwimmer(swimmerId);
  }

  [[nodiscard]] virtual auto hasSwimmer(const ::Swimmer::SwimmerId& swimmerId) const -> bool = 0;

  virtual void               addRelay(const Relay::Id& relayId)                              = 0;
  virtual auto               getRelay(const Relay::Id& relayId) -> Relay&                    = 0;
  virtual void               deleteRelay(const Relay::Id& relayId)                           = 0;
  [[nodiscard]] virtual auto hasRelay(const Relay::Id& relayId) const -> bool                = 0;

  [[nodiscard]] virtual auto isSwimmerAvailable(const ::Swimmer::SwimmerId& swimmerId) const -> bool
      = 0;

  void setSwimmerInRelayPosition(const ::Swimmer::SwimmerId& swimmerId, const Relay::Id& relayId,
                                 uint8_t position) {
    setSwimmerInRelayPosition(swimmerId, relayId, position, [](const Relay*) {});
  }

  void setSwimmerInRelayPosition(const ::Swimmer::SwimmerId& swimmerId, const Relay::Id& relayId,
                                 uint8_t                                  position,
                                 const std::function<void(const Relay*)>& callback) {
    getRelay(relayId).setSwimmer(pickSwimmer(swimmerId), position, callback);
  }

  auto pickSwimmer(const ::Swimmer::SwimmerId& swimmerId) -> std::unique_ptr<Swimmer> {
    return std::make_unique<Swimmer>(getSwimmer(swimmerId), *this);
  }

  [[nodiscard]] virtual auto getAvailableSwimmersForEvent(
      const Event& event, Times::Pool pool,
      const std::function<bool(const ::Swimmer& swimmer)>& isValid) const
      -> SwimmerEventCollection::Collection
      = 0;

  [[nodiscard]] auto getAvailableSwimmersForEvent(const Event& event, Times::Pool pool) const
      -> SwimmerEventCollection::Collection {
    return getAvailableSwimmersForEvent(event, pool, [](const ::Swimmer&) { return true; });
  }

  [[nodiscard]] virtual auto getRelayIterator() const -> std::unique_ptr<ConstRelayIterator> = 0;

protected:
  virtual void _removeSwimmer(const ::Swimmer::SwimmerId& swimmerId) = 0;
  virtual void useSwimmer(const ::Swimmer::SwimmerId& swimmerId)     = 0;
  virtual void unuseSwimmer(const ::Swimmer::SwimmerId& swimmerId)   = 0;

  void         addSwimmerRemoveNotifier(Swimmer& swimmer) {
    swimmerRemoveNotifiers_.emplace(swimmer.getId(), [&swimmer, this]() {
      swimmer.notifyDeleted(getSwimmer(swimmer.getId()));
    });
  }

  void removeSwimmerRemoveNotifier(Swimmer& swimmer) {
    swimmerRemoveNotifiers_.erase(swimmer.getId());
  }

private:
  SwimmersNotifyCollection swimmerRemoveNotifiers_;
};

inline auto operator<<(std::ostream& ostr, const Session::Swimmer& swimmer) -> std::ostream& {
  return ostr << "[Session::Swimmer id=" << swimmer.getId() << "]";
}

inline Session::Swimmer::Swimmer(const ::Swimmer& swimmer, Session& session) :
    SwimmerProxy(swimmer),
    session_(session) {
  session_.useSwimmer(SwimmerProxy::getId());
  session_.addSwimmerRemoveNotifier(*this);
  spdlog::debug("{} {} created", *this, static_cast<void*>(this));
}

inline Session::Swimmer::~Swimmer() {
  session_.unuseSwimmer(SwimmerProxy::getId());
  session_.removeSwimmerRemoveNotifier(*this);
  spdlog::debug("{} {} destroyed", *this, static_cast<void*>(this));
}

#endif
