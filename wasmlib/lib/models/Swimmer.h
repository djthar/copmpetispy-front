#ifndef COMPETYSPY_FRONT_SWIMMER_H
#define COMPETYSPY_FRONT_SWIMMER_H

#include <cstdint>
#include <string>
#include <unordered_set>

#include "nlohmann/json.hpp"
#include "spdlog/spdlog.h"

#include "Gender.h"
#include "SwimmerObserver.h"
#include "Times.h"

#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

class Swimmer;
inline auto operator<<(std::ostream& os, const Swimmer& c) -> std::ostream&;

class Swimmer {
  using SwimmerObservers = std::unordered_set<SwimmerObserver*>;

public:
  struct SwimmerId {
    SwimmerId(std::string name, std::string surname, uint16_t bornYear, Gender gender) :
        name(std::move(name)),
        surname(std::move(surname)),
        bornYear(bornYear),
        gender(gender) {}

    std::string name;
    std::string surname;
    uint16_t    bornYear;
    Gender      gender;

    auto        operator==(const SwimmerId& rhs) const -> bool {
      return name == rhs.name && surname == rhs.surname && bornYear == rhs.bornYear;
    }

    struct Hash {
      auto operator()(const SwimmerId& swimmer) const -> size_t {
        return this->operator()(swimmer.name, swimmer.surname, swimmer.bornYear);
      }

      auto operator()(const std::string& name, const std::string& surname, uint16_t bornYear) const
          -> size_t {
        std::size_t h = 0;
        hash_combine(h, name, surname, bornYear);
        return h;
      }
    };

    static void to_json(nlohmann::json& j, const Swimmer::SwimmerId& id) {
      j["name"]     = id.name;
      j["surname"]  = id.surname;
      j["bornYear"] = id.bornYear;
      j["gender"]   = id.gender;
    }

    static auto from_json(const nlohmann::json& j) -> Swimmer::SwimmerId {
      return {j.at("name").get<std::string>(), j.at("surname").get<std::string>(),
              j.at("bornYear").get<uint16_t>(), j.at("gender").get<Gender>()};
    }
  };

  Swimmer() : swimmerObservers_(std::make_unique<SwimmerObservers>()) {}

  Swimmer(Swimmer&& swimmer) noexcept : Swimmer() {
    *swimmerObservers_ = std::move(*swimmer.swimmerObservers_);
  }

  Swimmer(const Swimmer&) = delete;

  virtual ~Swimmer() {
    // spdlog::debug("[Swimmer {}] destroying", static_cast<void *>(this));
    // notifyDeletion();
    spdlog::debug("[Swimmer {}] destroyed", static_cast<void*>(this));
  }

  [[nodiscard]] virtual auto getTime(Event event, Times::Pool pool) const -> float            = 0;

  [[nodiscard]] virtual auto getTimeAutoConvert(Event event, Times::Pool pool) const -> float = 0;

  [[nodiscard]] virtual auto getAge(uint16_t actualYear) const -> uint16_t                    = 0;

  [[nodiscard]] virtual auto getName() const -> const std::string&                            = 0;

  [[nodiscard]] virtual auto getSurname() const -> const std::string&                         = 0;

  [[nodiscard]] virtual auto getBornYear() const -> uint16_t                                  = 0;

  [[nodiscard]] virtual auto getGender() const -> Gender                                      = 0;

  virtual void               setEventTime(const Event& event, float time, Times::Pool pool)   = 0;

  auto operator==(const Swimmer& rhs) const -> bool { return getId() == rhs.getId(); }

  [[nodiscard]] virtual auto getId() const -> const SwimmerId& = 0;

  virtual void               registerSwimmerObserver(SwimmerObserver& observer) const {
    if (nullptr == swimmerObservers_) { return; }
    spdlog::debug("{} register observer {}", *this, static_cast<const void*>(&observer));
    swimmerObservers_->emplace(&observer);
  }

  virtual void unregisterSwimmerObserver(SwimmerObserver& observer) const {
    if (nullptr == swimmerObservers_) { return; }
    spdlog::debug("{} unregister observer {}", *this, static_cast<const void*>(&observer));
    swimmerObservers_->erase(&observer);
  }

protected:
  void notifyDeletion() {
    if (swimmerObservers_ != nullptr) {
      auto observers = std::move(swimmerObservers_);
      spdlog::debug("[Swimmer {}] notifyDeletion to {} observers", static_cast<void*>(this),
                    observers->size());

      for (auto& observer : *observers) { observer->notifyDeleted(*this); }
    }
  }

  void notifyUpdated() {
    if (swimmerObservers_ != nullptr) {
      spdlog::debug("[Swimmer {}] notifyUpdated to {} observers", static_cast<void*>(this),
                    swimmerObservers_->size());

      for (auto& observer : *swimmerObservers_) { observer->notifyUpdated(*this); }
    }
  }

  std::unique_ptr<SwimmerObservers> swimmerObservers_;
};

inline auto operator<<(std::ostream& os, const Swimmer::SwimmerId& c) -> std::ostream& {
  return os << "{name=" << c.name << ", surname=" << c.surname << ", bornYear=" << c.bornYear
            << ", gender=" << c.gender << "}";
}

inline auto operator<<(std::ostream& os, const Swimmer& c) -> std::ostream& {
  return os << "[Swimmer id=" << c.getId() << "] " << static_cast<const void*>(&c);
}

#endif  // COMPETYSPY_FRONT_SWIMMER_H
