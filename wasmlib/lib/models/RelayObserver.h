#ifndef COMPETYSPY_FRONT_RELAY_OBSERVER_H
#define COMPETYSPY_FRONT_RELAY_OBSERVER_H

class RelayObserver {
public:
  virtual ~RelayObserver()     = default;
  virtual void notifyChanged() = 0;
  virtual void notifyRemoved() = 0;
};

#endif
