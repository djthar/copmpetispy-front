#ifndef COMPETYSPY_FRONT_SWIMMER_OBSERVER_H
#define COMPETYSPY_FRONT_SWIMMER_OBSERVER_H

// Forward declaration because Swimmer needs to know its observer to notify its
// destruction
class Swimmer;

class SwimmerObserver {
public:
  virtual ~SwimmerObserver()                         = default;
  virtual void notifyDeleted(const Swimmer& swimmer) = 0;
  virtual void notifyUpdated(const Swimmer& swimmer) = 0;
};

#endif
