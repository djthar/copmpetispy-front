#ifndef COMPETYSPY_FRONT_SESSION_LOCAL_H
#define COMPETYSPY_FRONT_SESSION_LOCAL_H

#include <memory>
#include <unordered_set>

#include "Event.h"
#include "Relay.h"
#include "RelayCategory.h"
#include "RelayLocal.h"
#include "Session.h"
#include "Swimmer.h"
#include "SwimmerProvider.h"
#include "SwimmerProxy.h"

class SessionLocal : public Session {
  using json               = nlohmann::json;
  using SwimmersCollection = std::unordered_set<::Swimmer::SwimmerId, ::Swimmer::SwimmerId::Hash>;
  using RelayCollection    = std::vector<std::unique_ptr<RelayLocal>>;

  class SwimmersCollectionIterator : public ConstSwimmerIterator {
  public:
    explicit SwimmersCollectionIterator(const SessionLocal& session) :
        session_(session),
        it_(session.swimmers_.begin()) {}

    auto getNext() -> const ::Swimmer& override {
      const auto& swimmer = session_.getSwimmer(*it_);
      if (hasMore()) { ++it_; }
      return swimmer;
    }

    [[nodiscard]] auto hasMore() const -> bool override { return it_ != session_.swimmers_.end(); }

  private:
    const SessionLocal&                session_;
    SwimmersCollection::const_iterator it_;
  };

  class RelayCollectionIterator : public ConstRelayIterator {
  public:
    explicit RelayCollectionIterator(const SessionLocal& session) :
        it_(session.relays_.begin()),
        end_(session.relays_.end()) {}

    auto getNext() -> const Relay& override {
      const auto& relay = **it_;
      if (hasMore()) { ++it_; }
      return relay;
    }

    [[nodiscard]] auto hasMore() const -> bool override { return it_ != end_; }

  private:
    RelayCollection::const_iterator       it_;
    const RelayCollection::const_iterator end_;
  };

public:
  explicit SessionLocal(SwimmerProvider& swimmerProvider) : swimmerProvider_(swimmerProvider) {}

  SessionLocal(const nlohmann::json& j, SwimmerProvider& swimmerProvider);

  SessionLocal(SessionLocal&&)      = default;
  SessionLocal(const SessionLocal&) = delete;
  ~SessionLocal() override          = default;

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const ::Swimmer& override;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) -> ::Swimmer& override;

  void               addSwimmer(const ::Swimmer::SwimmerId& swimmer) override;
  void               _removeSwimmer(const ::Swimmer::SwimmerId& swimmer) override;
  auto               hasSwimmer(const ::Swimmer::SwimmerId& swimmer) const -> bool override;

  void               addRelay(const Relay::Id& id) override;
  auto               getRelay(const Relay::Id& id) -> Relay& override;
  auto               getRelay(const Relay::Id& id) const -> const Relay&;
  void               deleteRelay(const Relay::Id& id) override;
  auto               hasRelay(const Relay::Id& id) const -> bool override;

  auto               isSwimmerAvailable(const ::Swimmer::SwimmerId& swimmer) const -> bool override;

  [[nodiscard]] auto getAvailableSwimmersForEvent(
      const Event& event, Times::Pool pool, const std::function<bool(const ::Swimmer&)>& isValid)
      const -> SwimmerEventCollection::Collection override;

  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override {
    return std::make_unique<SwimmersCollectionIterator>(*this);
  }

  [[nodiscard]] auto getRelayIterator() const -> std::unique_ptr<ConstRelayIterator> override {
    return std::make_unique<RelayCollectionIterator>(*this);
  }

  operator json() const;

protected:
  void useSwimmer(const ::Swimmer::SwimmerId& swimmer) override;
  void unuseSwimmer(const ::Swimmer::SwimmerId& swimmer) override;

  auto findRelay(const Relay::Id& id) {
    return std::find_if(
        relays_.begin(), relays_.end(),
        [&id](const std::unique_ptr<RelayLocal>& relay) { return relay->getId() == id; });
  }

  auto findRelay(const Relay::Id& id) const {
    return std::find_if(
        relays_.begin(), relays_.end(),
        [&id](const std::unique_ptr<RelayLocal>& relay) { return relay->getId() == id; });
  }

private:
  SwimmerProvider&   swimmerProvider_;
  SwimmersCollection swimmers_;
  RelayCollection    relays_;
  SwimmersCollection usedSwimmers_;
};

#endif
