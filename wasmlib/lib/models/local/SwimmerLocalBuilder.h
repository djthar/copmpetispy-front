#ifndef COMPETYSPY_FRONT_SWIMMER_LOCAL_BUILDER_H
#define COMPETYSPY_FRONT_SWIMMER_LOCAL_BUILDER_H

#include <algorithm>
#include <memory>
#include <string>
#include <vector>

#include "Gender.h"
#include "Swimmer.h"
#include "SwimmerLocal.h"
#include "Times.h"

class SwimmerLocalBuilder {
public:
  SwimmerLocalBuilder() = default;

  auto name(std::string n) -> SwimmerLocalBuilder& {
    name_ = std::move(n);
    return *this;
  }

  auto surname(std::string n) -> SwimmerLocalBuilder& {
    surname_ = std::move(n);
    return *this;
  }

  auto bornYear(uint16_t n) -> SwimmerLocalBuilder& {
    bornYear_ = n;
    return *this;
  }

  auto age(uint16_t year, uint16_t value) -> SwimmerLocalBuilder& { return bornYear(year - value); }

  auto gender(Gender n) -> SwimmerLocalBuilder& {
    gender_ = n;
    return *this;
  }

  auto time(Event event, float t, Times::Pool pool = Times::_25) -> SwimmerLocalBuilder& {
    times_.emplace_back(event, t, pool);
    return *this;
  }

  auto build() -> SwimmerLocal {
    SwimmerLocal swimmer(name_, surname_, bornYear_, gender_);
    for (const auto& [event, t, pool] : times_) { swimmer.setEventTime(event, t, pool); }
    return swimmer;
  }

  auto buildPtr() -> std::unique_ptr<SwimmerLocal> {
    return std::make_unique<SwimmerLocal>(build());
  }

private:
  std::string                                        name_     = "Name";
  std::string                                        surname_  = "Surname";
  uint16_t                                           bornYear_ = 1980;
  Gender                                             gender_   = Gender::MALE;
  std::vector<std::tuple<Event, float, Times::Pool>> times_;
};

#endif
