#ifndef COMPETYSPY_FRONT_RELAY_LOCAL_H
#define COMPETYSPY_FRONT_RELAY_LOCAL_H

#include <functional>
#include <unordered_set>

#include "nlohmann/json_fwd.hpp"

#include "Event.h"
#include "Relay.h"
#include "RelayCategory.h"
#include "RelayObserver.h"
#include "SwimmerObserver.h"
#include "Times.h"

class RelayLocal : public Relay, public SwimmerObserver {
  using StylesType = std::array<Style, 4>;
  using json       = nlohmann::json;

public:
  RelayLocal(Event event, RelayCategory category, Times::Pool pool = Times::Pool::_25);
  RelayLocal(Relay::Id relayId);
  RelayLocal(
      const nlohmann::json&                                                        j,
      const std::function<std::unique_ptr<Swimmer>(const Swimmer::SwimmerId& id)>& getSwimmer);

  RelayLocal(RelayLocal&&)                   = delete;
  auto operator=(RelayLocal&) -> RelayLocal& = delete;
  ~RelayLocal() override;

  void               setSwimmer(std::unique_ptr<Swimmer> swimmer, uint8_t position,
                                const std::function<void(const Relay*)>& callback) override;

  [[nodiscard]] auto getId() const -> Id override;

  [[nodiscard]] auto getEvent() const -> Event override;

  [[nodiscard]] auto getCategory() const -> RelayCategory override;

  auto               removeSwimmer(uint8_t position) -> std::unique_ptr<Swimmer> override;
  void               clean() override;
  [[nodiscard]] auto getPool() const -> Times::Pool override;

  void visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) override;
  void visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) const override;

  [[nodiscard]] auto isFull() const -> bool override;

  operator json();

  void notifyDeleted(const Swimmer& swimmer) override;
  void notifyUpdated(const Swimmer& swimmer) override;
  void registerRelayObserver(RelayObserver* relayObserver) override;
  void unregisterRelayObserver(RelayObserver* relayObserver) override;
  void notifyRelayChanged() const;

protected:
  static constexpr auto getStyleArray(Style style) -> StylesType {
    switch (style) {
      case Style::BUTTERFLY :
        return {Style::BUTTERFLY, Style::BUTTERFLY, Style::BUTTERFLY, Style::BUTTERFLY};
      case Style::BACK :
        return {Style::BACK, Style::BACK, Style::BACK, Style::BACK};
      case Style::BREAST :
        return {Style::BREAST, Style::BREAST, Style::BREAST, Style::BREAST};
      case Style::FREE :
        return {Style::FREE, Style::FREE, Style::FREE, Style::FREE};
      case Style::MEDLEY :
        return {Style::BACK, Style::BREAST, Style::BUTTERFLY, Style::FREE};
    }
    return {};
  };

  [[nodiscard]] auto getEventForPosition(size_t position) const -> Event override;

  void               unregisterSwimmerDestroyObserver(uint8_t position);

  Relay::Id          relayId_;
  StylesType         styles_{};
  std::array<std::unique_ptr<Swimmer>, 4> swimmers_{};
  std::unordered_set<RelayObserver*>      relayChangedObservers_;
};

inline auto operator<<(std::ostream& os, const RelayLocal& c) -> std::ostream& {
  nlohmann::json j;
  j["event"]    = c.getEvent();
  j["category"] = c.getCategory();
  j["pool"]     = c.getPool();
  return os << "[RelayLocal " << j.dump() << "] " << static_cast<const void*>(&c);
}

#endif  // COMPETYSPY_FRONT_RELAY_LOCAL_H
