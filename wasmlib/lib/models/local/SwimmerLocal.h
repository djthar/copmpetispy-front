#ifndef COMPETYSPY_FRONT_SWIMMER_LOCAL_H
#define COMPETYSPY_FRONT_SWIMMER_LOCAL_H

#include <cstdint>
#include <string>

#include "nlohmann/json.hpp"
#include "nlohmann/json_fwd.hpp"
#include "spdlog/spdlog.h"

#include "Gender.h"
#include "Swimmer.h"
#include "Times.h"

#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

class SwimmerLocal;
inline auto operator<<(std::ostream& ostr, const SwimmerLocal& swimmer) -> std::ostream&;

class SwimmerLocal : public Swimmer {
  using json = nlohmann::json;

public:
  SwimmerLocal(std::string name, std::string surname, uint16_t bornYear, Gender gender);
  SwimmerLocal(const nlohmann::json& jsonValue);
  SwimmerLocal(const SwimmerLocal&) = delete;
  explicit SwimmerLocal(Swimmer::SwimmerId swimmerId);
  SwimmerLocal(SwimmerLocal&& swimmer) noexcept;
  ~SwimmerLocal() override;

  [[nodiscard]] auto getTime(Event event, Times::Pool pool) const -> float override;

  [[nodiscard]] auto getTimeAutoConvert(Event event, Times::Pool pool) const -> float override;

  [[nodiscard]] auto getAge(uint16_t actualYear) const -> uint16_t override;

  [[nodiscard]] auto getName() const -> const std::string& override;

  [[nodiscard]] auto getSurname() const -> const std::string& override;

  [[nodiscard]] auto getBornYear() const -> uint16_t override;

  [[nodiscard]] auto getGender() const -> Gender override;

  void               setEventTime(const Event& event, float time, Times::Pool pool) override;

  auto               operator==(const SwimmerLocal& rhs) const -> bool;

  [[nodiscard]] auto getId() const -> const Swimmer::SwimmerId& override;

  operator json() const;

private:
  Swimmer::SwimmerId id_;
  Times              times_;
};

inline auto operator<<(std::ostream& ostr, const SwimmerLocal& swimmer) -> std::ostream& {
  return ostr << "[SwimmerLocal id=" << swimmer.getId() << "] "
              << static_cast<const void*>(&swimmer);
}

namespace nlohmann {
template<>
struct adl_serializer<Swimmer::SwimmerId> {
  static auto from_json(const json& jsonValue) -> Swimmer::SwimmerId {
    return Swimmer::SwimmerId::from_json(jsonValue);
  }

  static void to_json(json& jsonValue, const Swimmer::SwimmerId& swimmerId) {
    Swimmer::SwimmerId::to_json(jsonValue, swimmerId);
  }
};

template<>
struct adl_serializer<SwimmerLocal> {
  static auto from_json(const json& jsonValue) -> SwimmerLocal { return {jsonValue}; }

  static void to_json(json& jsonValue, const SwimmerLocal& swimmer) { jsonValue = swimmer; }
};
}  // namespace nlohmann

#endif  // COMPETYSPY_FRONT_SWIMMER_LOCAL_H
