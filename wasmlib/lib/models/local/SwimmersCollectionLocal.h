#ifndef COMPETYSPY_FRONT_SWIMMERS_COLLECTION_LOCAL_H
#define COMPETYSPY_FRONT_SWIMMERS_COLLECTION_LOCAL_H

#include <memory>
#include <vector>

#include "nlohmann/json.hpp"

#include "Swimmer.h"
#include "SwimmerLocal.h"
#include "SwimmersCollection.h"

class SwimmersCollectionLocal : public SwimmersCollection {
  using SwimmersCollectionT = std::unordered_map<Swimmer::SwimmerId, std::unique_ptr<SwimmerLocal>,
                                                 Swimmer::SwimmerId::Hash>;

  class SwimmersCollectionIterator : public ConstSwimmerIterator {
  public:
    explicit SwimmersCollectionIterator(const SwimmersCollectionT& collection) :
        it_(collection.begin()),
        end_(collection.end()) {}

    auto getNext() -> const Swimmer& override {
      if (hasMore()) { ++it_; }
      return *(it_->second);
    }

    [[nodiscard]] auto hasMore() const -> bool override { return it_ != end_; }

  private:
    SwimmersCollectionT::const_iterator       it_;
    const SwimmersCollectionT::const_iterator end_;
  };

public:
  SwimmersCollectionLocal()                                     = default;
  SwimmersCollectionLocal(SwimmersCollectionLocal&& collection) = default;
  ~SwimmersCollectionLocal() override                           = default;

  void addSwimmer(Swimmer::SwimmerId id) override;

  void addSwimmer(std::string name, std::string surname, uint16_t bornYear, Gender gender) override;

  void removeSwimmer(const Swimmer::SwimmerId& id) override;

  void removeSwimmer(std::string name, std::string surname, uint16_t bornYear,
                     Gender gender) override;

  [[nodiscard]] auto hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool override;

  void               cleanAllSwimmers() override;

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override;

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override;

  auto               getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override {
    return std::make_unique<SwimmersCollectionIterator>(swimmers_);
  }

  auto        size() const -> size_t { return swimmers_.size(); }

  friend void to_json(nlohmann::json& j, const SwimmersCollectionLocal& t) {
    for (const auto& swimmer : t.swimmers_) { j.push_back(*swimmer.second); }
  }

  friend void from_json(const nlohmann::json& j, SwimmersCollectionLocal& t) {
    for (const auto& swimmerJson : j) {
      auto               swimmer = std::make_unique<SwimmerLocal>(swimmerJson);
      Swimmer::SwimmerId id      = swimmer->getId();
      t.swimmers_.emplace(std::move(id), std::move(swimmer));
    }
  }

private:
  SwimmersCollectionT swimmers_;
};

#endif
