#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <iostream>
#include <stdexcept>

#include "nlohmann/json.hpp"
#include "nlohmann/json_fwd.hpp"

#include "Session.h"
#include "SwimmerLocalBuilder.h"
#include "SwimmerProxyProvider.h"
#include "TeamLocal.h"

class TeamLocalTest : public ::testing::Test {
public:
  TeamLocalTest() : team(swimmerProvider) {}

protected:
  SwimmerProxyProvider swimmerProvider;
  TeamLocal            team;
};

TEST_F(TeamLocalTest, GIVEN_a_team_with_1_swimmer_WHEN_check_available_THEN_is_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  EXPECT_TRUE(team.hasSwimmer(swimmerProvider.getSwimmer("sw1").getId()));
}

TEST_F(TeamLocalTest, GIVEN_an_empty_team_WHEN_dump_and_restore_THEN_no_throw) {
  EXPECT_NO_THROW(TeamLocal(static_cast<nlohmann::json>(team), swimmerProvider));
}

TEST_F(TeamLocalTest, GIVEN_a_team_with_1_swimmer_WHEN_dump_and_restore_THEN_is_available) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  TeamLocal recoveredTeam(static_cast<nlohmann::json>(team), swimmerProvider);
  EXPECT_TRUE(recoveredTeam.hasSwimmer(swimmerProvider.getSwimmer("sw1").getId()));
}

TEST_F(TeamLocalTest,
       GIVEN_a_team_with_1_session_WHEN_dump_and_restore_THEN_same_session_is_available) {
  team.addSession("1");
  TeamLocal recoveredTeam(static_cast<nlohmann::json>(team), swimmerProvider);
  EXPECT_NO_THROW(recoveredTeam.getSession("1"));
}

TEST_F(
    TeamLocalTest,
    GIVEN_a_team_with_1_session_with_one_swimmer_WHEN_dump_and_restore_THEN_recovered_session_has_same_swimmer) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  team.addSession("1");
  team.addSwimmerToSession(swimmerProvider.getSwimmer("sw1"), "1");
  TeamLocal recoveredTeam(static_cast<nlohmann::json>(team), swimmerProvider);
  EXPECT_TRUE(recoveredTeam.getSession("1").hasSwimmer(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(TeamLocalTest, GIVEN_a_team_with_a_removed_swimmer_WHEN_get_it_THEN_throws) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  team.removeSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  EXPECT_THROW(auto& swimmer = team.getSwimmer(swimmerProvider.getSwimmer("sw1").getId()),
               std::logic_error);
}

TEST_F(
    TeamLocalTest,
    GIVEN_a_team_with_a_session_with_a_removed_swimmer_WHEN_check_available_in_session_THEN_is_not) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  team.addSession("1");
  team.addSwimmerToSession(swimmerProvider.getSwimmer("sw1"), "1");
  team.removeSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  EXPECT_FALSE(team.getSession("1").hasSwimmer(swimmerProvider.getSwimmerId("sw1")));
}

TEST_F(
    TeamLocalTest,
    GIVEN_a_team_backup_with_a_session_with_a_swimmer_WHEN_recover_THEN_it_has_same_session_with_same_swimmer) {
  swimmerProvider.add("sw1", SwimmerLocalBuilder().build());
  team.addSwimmer(swimmerProvider.getSwimmer("sw1").getId());
  team.addSession("1");
  team.addSwimmerToSession(swimmerProvider.getSwimmer("sw1"), "1");
  const auto& teamJson = static_cast<nlohmann::json>(team);
  TeamLocal   recoveredTeam(teamJson, swimmerProvider);
  EXPECT_TRUE(recoveredTeam.hasSwimmer(swimmerProvider.getSwimmer("sw1").getId()));
  ASSERT_NO_THROW(recoveredTeam.getSession("1"));
  EXPECT_TRUE(recoveredTeam.getSession("1").hasSwimmer(swimmerProvider.getSwimmerId("sw1")));
}
