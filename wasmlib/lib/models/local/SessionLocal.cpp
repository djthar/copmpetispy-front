#include "SessionLocal.h"

#include <functional>
#include <memory>
#include <stdexcept>
#include <vector>

#include "spdlog/spdlog.h"

#include "RelayLocal.h"
#include "Swimmer.h"
#include "SwimmerEventCollection.h"
#include "SwimmerLocal.h"

using SwimmerId = ::Swimmer::SwimmerId;

SessionLocal::SessionLocal(const nlohmann::json& j, SwimmerProvider& swimmerProvider) :
    SessionLocal(swimmerProvider) {
  if (j.contains("swimmers")) {
    for (const auto& swimmerIdJson : j["swimmers"]) {
      const auto swimmerId = swimmerIdJson.get<SwimmerId>();
      swimmers_.emplace(swimmerId);
    }
  }
  if (j.contains("relays")) {
    for (const auto& relayJson : j["relays"]) {
      relays_.emplace_back(std::make_unique<RelayLocal>(
          relayJson, [this](const ::Swimmer::SwimmerId& id) { return pickSwimmer(id); }));
    }
  }
}

auto SessionLocal::getSwimmer(const Swimmer::SwimmerId& id) const -> const ::Swimmer& {
  return swimmerProvider_.getSwimmer(id);
}

auto SessionLocal::getSwimmer(const Swimmer::SwimmerId& id) -> ::Swimmer& {
  return swimmerProvider_.getSwimmer(id);
}

void SessionLocal::addSwimmer(const ::Swimmer::SwimmerId& swimmer) { swimmers_.emplace(swimmer); }

void SessionLocal::_removeSwimmer(const ::Swimmer::SwimmerId& swimmer) {
  spdlog::debug("[SessionLocal::_removeSwimmer]");
  swimmers_.erase(swimmer);
}

auto SessionLocal::hasSwimmer(const ::Swimmer::SwimmerId& swimmer) const -> bool {
  return swimmers_.contains(swimmer);
}

void SessionLocal::addRelay(const Relay::Id& id) {
  if (hasRelay(id)) { return; }
  relays_.emplace_back(std::make_unique<RelayLocal>(id));
}

auto SessionLocal::getRelay(const Relay::Id& id) -> Relay& {
  if (const auto it = findRelay(id); it != relays_.end()) { return **it; }
  throw std::logic_error{"Relay not found"};
}

auto SessionLocal::getRelay(const Relay::Id& id) const -> const Relay& {
  if (const auto it = findRelay(id); it != relays_.end()) { return **it; }
  throw std::logic_error{"Relay not found"};
}

void SessionLocal::deleteRelay(const Relay::Id& id) { relays_.erase(findRelay(id)); }

auto SessionLocal::hasRelay(const Relay::Id& id) const -> bool {
  return findRelay(id) != relays_.end();
}

void SessionLocal::useSwimmer(const ::Swimmer::SwimmerId& swimmer) {
  if (not isSwimmerAvailable(swimmer)) {
    throw std::logic_error("Trying to use a swimmer that doesnt belog to the session or that is "
                           "already used");
  }
  usedSwimmers_.emplace(swimmer);
}

void SessionLocal::unuseSwimmer(const ::Swimmer::SwimmerId& swimmer) {
  if (not hasSwimmer(swimmer)) {
    throw std::logic_error("Trying to use a swimmer that doesnt belog to the session");
  }
  usedSwimmers_.erase(swimmer);
}

auto SessionLocal::isSwimmerAvailable(const ::Swimmer::SwimmerId& swimmer) const -> bool {
  return hasSwimmer(swimmer) and not usedSwimmers_.contains(swimmer);
}

[[nodiscard]] auto SessionLocal::getAvailableSwimmersForEvent(
    const Event& event, Times::Pool pool,
    const std::function<bool(const ::Swimmer&)>& isValid) const
    -> SwimmerEventCollection::Collection {
  std::vector<std::reference_wrapper<const ::Swimmer>> availableSwimmers;
  for (const auto& swimmerId : swimmers_) {
    if (usedSwimmers_.contains(swimmerId)) { continue; }
    availableSwimmers.emplace_back(swimmerProvider_.getSwimmer(swimmerId));
  }
  return SwimmerEventCollection::createSortedCollection(availableSwimmers, event, pool, isValid);
}

SessionLocal::operator nlohmann::json() const {
  nlohmann::json j;
  for (const auto& swimmerId : swimmers_) { j["swimmers"].emplace_back(swimmerId); }
  for (const auto& relay : relays_) { j["relays"].emplace_back(*relay); }
  return j;
}
