#include "TeamLocal.h"

#include <stdexcept>

#include "SwimmerLocal.h"

TeamLocal::TeamLocal(SwimmerProvider& swimmerProvider) : swimmerProvider_(&swimmerProvider) {}

TeamLocal::TeamLocal(const json& jsonValue, SwimmerProvider& swimmerProvider) :
    TeamLocal(swimmerProvider) {
  if (jsonValue.contains("swimmers")) {
    for (const auto& swimmerJson : jsonValue["swimmers"]) {
      const auto& swimmer = swimmerProvider_->getSwimmer(swimmerJson.get<Swimmer::SwimmerId>());
      swimmers_.emplace(swimmer.getId());
    }
  }
  for (const auto& [sessionId, sessionJson] : jsonValue["sessions"].items()) {
    sessions_.try_emplace(sessionId, sessionJson, *this);
  }
}

void TeamLocal::addSession(const std::string& sessionId) {
  sessions_.emplace(sessionId, *swimmerProvider_);
}

void TeamLocal::removeSession(const std::string& sessionId) { sessions_.erase(sessionId); }

auto TeamLocal::getSession(const std::string& sessionId) -> Session& {
  return sessions_.at(sessionId);
}

auto TeamLocal::getSession(const std::string& sessionId) const -> const Session& {
  return sessions_.at(sessionId);
}

auto TeamLocal::hasSession(const std::string& sessionId) const -> bool {
  return sessions_.contains(sessionId);
}

auto TeamLocal::getSessionIds() const -> std::vector<std::string> {
  std::vector<std::string> sessionIds;
  sessionIds.reserve(sessions_.size());
  for (const auto& session : sessions_) { sessionIds.emplace_back(session.first); }
  return sessionIds;
}

void TeamLocal::addSwimmer(const Swimmer::SwimmerId& swimmerId) { swimmers_.emplace(swimmerId); }

void TeamLocal::removeSwimmer(const Swimmer::SwimmerId& swimmerId) {
  swimmers_.erase(swimmerId);
  for (auto& [sessionId, session] : sessions_) { session.removeSwimmer(swimmerId); }
}

[[nodiscard]] auto TeamLocal::hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool {
  return swimmers_.contains(swimmerId);
}

[[nodiscard]] auto TeamLocal::getSwimmer(const Swimmer::SwimmerId& swimmerId) const
    -> const Swimmer& {
  if (not hasSwimmer(swimmerId)) { throw std::logic_error("Swimmer not available in team"); }
  return swimmerProvider_->getSwimmer(swimmerId);
}

[[nodiscard]] auto TeamLocal::getSwimmer(const Swimmer::SwimmerId& swimmerId) -> Swimmer& {
  if (not hasSwimmer(swimmerId)) { throw std::logic_error("Swimmer not available in team"); }
  return swimmerProvider_->getSwimmer(swimmerId);
}

TeamLocal::operator json() const {
  json jsonValue;
  for (const auto& swimmer : swimmers_) { jsonValue["swimmers"].emplace_back(swimmer); }
  jsonValue["sessions"] = sessions_;
  return jsonValue;
}
