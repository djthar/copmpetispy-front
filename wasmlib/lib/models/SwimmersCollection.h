#ifndef COMPETYSPY_FRONT_SWIMMERS_COLLECTION_H
#define COMPETYSPY_FRONT_SWIMMERS_COLLECTION_H

#include "Swimmer.h"
#include "SwimmerProvider.h"

class SwimmersCollection : public SwimmerProvider {
public:
  virtual void addSwimmer(Swimmer::SwimmerId id) = 0;

  virtual void addSwimmer(std::string name, std::string surname, uint16_t bornYear, Gender gender)
      = 0;

  virtual void removeSwimmer(const Swimmer::SwimmerId& id) = 0;

  virtual void removeSwimmer(std::string name, std::string surname, uint16_t bornYear,
                             Gender gender)
      = 0;

  virtual void               cleanAllSwimmers()                                                = 0;

  [[nodiscard]] virtual auto hasSwimmer(const Swimmer::SwimmerId& swimmerId) const -> bool     = 0;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override = 0;
  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override             = 0;
  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override
                                                   = 0;
};

#endif
