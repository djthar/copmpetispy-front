#ifndef COMPETYSPY_FRONT_TIMES_H
#define COMPETYSPY_FRONT_TIMES_H

#include <nlohmann/json.hpp>
#include <unordered_map>

#include "Event.h"
#include "Style.h"
#include "StyleTimes.h"
#include "TimeConverter.h"

class Times {
public:
  enum Pool {
    _25 = 0,
    _50 = 1
  };

  Times() {
    for (auto& time : times_) {
      time.emplace(Style::BUTTERFLY, Style::BUTTERFLY);
      time.emplace(Style::BACK, Style::BACK);
      time.emplace(Style::BREAST, Style::BREAST);
      time.emplace(Style::FREE, Style::FREE);
      time.emplace(Style::MEDLEY, Style::MEDLEY);
    }
  }

  ~Times()       = default;
  Times(Times&&) = default;

  [[nodiscard]] auto getTime(const Event& event, Pool pool = _25) const -> float {
    return times_[pool].at(event.style).getTimeForDistance(event.distance);
  }

  [[nodiscard]] auto getTimeAutoConvert(const Event& event, Pool pool) const -> float {
    float time = times_[pool].at(event.style).getTimeForDistance(event.distance);
    if (time == StyleTimes::NO_TIME) {
      time = times_[pool == _25 ? _50 : _25].at(event.style).getTimeForDistance(event.distance);
      if (time != StyleTimes::NO_TIME) {
        return pool == _25 ? TimeConverter::convertToSC(event, time)
                           : TimeConverter::convertToLC(event, time);
      }
    }
    return time;
  }

  void setEventTime(const Event& event, float time, Pool pool) {
    times_[pool].at(event.style).setTimeForDistance(event.distance, time);
  }

  friend void to_json(nlohmann::json& j, const Times& times) {
    j["25"] = times.times_[Times::Pool::_25];
    j["50"] = times.times_[Times::Pool::_50];
  }

  friend void from_json(const nlohmann::json& j, Times& times) {
    j.at("25").get_to(times.times_[Times::Pool::_25]);
    j.at("50").get_to(times.times_[Times::Pool::_50]);
  }

private:
  std::unordered_map<Style, StyleTimes> times_[2];
};

NLOHMANN_JSON_SERIALIZE_ENUM(Times::Pool, {
                                            {Times::Pool::_25, "25"},
                                            {Times::Pool::_50, "50"},
})

#endif  // COMPETYSPY_FRONT_TIMES_H
