#ifndef COMPETYSPY_FRONT_SWIMMER_PROXY_H
#define COMPETYSPY_FRONT_SWIMMER_PROXY_H

#include <functional>
#include <stdexcept>

#include "spdlog/spdlog.h"

#include "Swimmer.h"
#include "SwimmerObserver.h"

#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

class SwimmerProxy;
inline auto operator<<(std::ostream& ostr, const SwimmerProxy& swimmer) -> std::ostream&;

class SwimmerProxy : public Swimmer, public SwimmerObserver {
public:
  explicit SwimmerProxy(const Swimmer& swimmer) : swimmer_(swimmer) {
    spdlog::debug("{} created", *this);
    swimmer.registerSwimmerObserver(*this);
  }

  SwimmerProxy(SwimmerProxy&& swimmer) noexcept : swimmer_(swimmer.swimmer_) {
    spdlog::debug("{} move created", *this);
    *swimmerObservers_ = std::move(*swimmer.swimmerObservers_);
  }

  auto operator=(SwimmerProxy&& swimmer) noexcept -> SwimmerProxy& {
    spdlog::debug("{} moved", *this);
    swimmer_           = swimmer.swimmer_;
    *swimmerObservers_ = std::move(*swimmer.swimmerObservers_);
    return *this;
  }

  SwimmerProxy(const SwimmerProxy& swimmer)                    = delete;
  auto operator=(const SwimmerProxy& swimmer) -> SwimmerProxy& = delete;

  ~SwimmerProxy() override {
    spdlog::debug("{} destroying", *this);
    swimmer_.get().unregisterSwimmerObserver(*this);
    notifyDeletion();

    spdlog::debug("{} destroyed", *this);
  }

  void notifyDeleted(const Swimmer& swimmer) override {
    spdlog::debug("{} notified deleted. this[{}] source[{}]", *this, static_cast<void*>(this),
                  static_cast<const void*>(&swimmer));
    if (&swimmer != this) {
      spdlog::debug("{} notifying deletion", *this);
      notifyDeletion();
    }
  }

  void notifyUpdated(const Swimmer& swimmer) override {
    spdlog::debug("{} notified updated. this[{}] source[{}]", *this, static_cast<void*>(this),
                  static_cast<const void*>(&swimmer));
    if (&swimmer != this) {
      spdlog::debug("{} notifying update", *this);
      Swimmer::notifyUpdated();
    }
  }

  [[nodiscard]] auto getTime(Event event, Times::Pool pool) const -> float override {
    return swimmer_.get().getTime(event, pool);
  }

  [[nodiscard]] auto getTimeAutoConvert(Event event, Times::Pool pool) const -> float override {
    return swimmer_.get().getTimeAutoConvert(event, pool);
  }

  [[nodiscard]] auto getAge(uint16_t actualYear) const -> uint16_t override {
    return swimmer_.get().getAge(actualYear);
  }

  [[nodiscard]] auto getName() const -> const std::string& override {
    return swimmer_.get().getName();
  }

  [[nodiscard]] auto getSurname() const -> const std::string& override {
    return swimmer_.get().getSurname();
  }

  [[nodiscard]] auto getBornYear() const -> uint16_t override {
    return swimmer_.get().getBornYear();
  }

  [[nodiscard]] auto getGender() const -> Gender override { return swimmer_.get().getGender(); }

  void               setEventTime(const Event& event, float time, Times::Pool pool) override {
    throw std::domain_error::logic_error("Trying to set time from SwimmerProxy is not allowed");
  }

  auto operator==(const SwimmerProxy& rhs) const -> bool {
    return swimmer_.get() == rhs.swimmer_.get();
  }

  [[nodiscard]] auto getId() const -> const Swimmer::SwimmerId& override {
    return swimmer_.get().getId();
  }

private:
  std::reference_wrapper<const Swimmer> swimmer_;
};

inline auto operator<<(std::ostream& ostr, const SwimmerProxy& swimmer) -> std::ostream& {
  return ostr << "[SwimmerProxy id=" << swimmer.getId() << "] "
              << static_cast<const void*>(&swimmer);
}

#endif  // COMPETYSPY_FRONT_SWIMMER_PROXY_H
