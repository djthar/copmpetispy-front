#ifndef COMPETYSPY_FRONT_GENDER_H
#define COMPETYSPY_FRONT_GENDER_H

#include <nlohmann/json.hpp>

#include "spdlog/spdlog.h"    // IWYU pragma: keep
#include "spdlog/fmt/ostr.h"  // IWYU pragma: keep

enum class Gender {
  MALE,
  FEMALE
};

NLOHMANN_JSON_SERIALIZE_ENUM(Gender, {
                                       {  Gender::MALE,   "male"},
                                       {Gender::FEMALE, "female"},
})

inline auto operator<<(std::ostream& os, const Gender& gender) -> std::ostream& {
  return os << (gender == Gender::MALE ? "male" : "female");
}

#endif  // COMPETYSPY_FRONT_GENDER_H
