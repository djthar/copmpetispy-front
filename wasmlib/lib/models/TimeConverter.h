#include "Event.h"
#include "Style.h"

class TimeConverter {
public:
  static float convertToLC(const Event& event, float time) { return time + getEventChange(event); }

  static float convertToSC(const Event& event, float time) { return time - getEventChange(event); }

private:
  static float getEventChange(const Event& event) {
    switch (event.gender) {
      case EventGender::MALE :
        switch (event.style) {
          case Style::BUTTERFLY :
            switch (event.distance) {
              case 50 :
                return 0.3;
              case 100 :
                return 1.3;
              case 200 :
                return 3.1;
              default :
                return 0;
            }
          case Style::BACK :
            switch (event.distance) {
              case 50 :
                return 1.1;
              case 100 :
                return 2.5;
              case 200 :
                return 5.7;
              default :
                return 0;
            }
          case Style::BREAST :
            switch (event.distance) {
              case 50 :
                return 0.8;
              case 100 :
                return 2.3;
              case 200 :
                return 6;
              default :
                return 0;
            }
          case Style::FREE :
            switch (event.distance) {
              case 50 :
                return 0.7;
              case 100 :
                return 1.6;
              case 200 :
                return 3.4;
              case 400 :
                return 7.2;
              case 800 :
                return 15.7;
              case 1500 :
                return 29.5;
              default :
                return 0;
            }
          case Style::MEDLEY :
            switch (event.distance) {
              case 200 :
                return 4.9;
              case 400 :
                return 10;
              default :
                return 0;
            }
        }
      case EventGender::FEMALE :
        switch (event.style) {
          case Style::BUTTERFLY :
            switch (event.distance) {
              case 50 :
                return 0.3;
              case 100 :
                return 0.8;
              case 200 :
                return 2.4;
              default :
                return 0;
            }
          case Style::BACK :
            switch (event.distance) {
              case 50 :
                return 1;
              case 100 :
                return 2.2;
              case 200 :
                return 5.7;
              default :
                return 0;
            }
          case Style::BREAST :
            switch (event.distance) {
              case 50 :
                return 0.6;
              case 100 :
                return 2;
              case 200 :
                return 4.5;
              default :
                return 0;
            }
          case Style::FREE :
            switch (event.distance) {
              case 50 :
                return 0.4;
              case 100 :
                return 1;
              case 200 :
                return 2.4;
              case 400 :
                return 5.2;
              case 800 :
                return 11.9;
              case 1500 :
                return 22.3;
              default :
                return 0;
            }
          case Style::MEDLEY :
            switch (event.distance) {
              case 200 :
                return 3.1;
              case 400 :
                return 7.5;
              default :
                return 0;
            }
        }
      default :
        return 0;
    }
  }
};
