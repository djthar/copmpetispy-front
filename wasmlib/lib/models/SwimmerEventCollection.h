#ifndef COMPETYSPY_FRONT_SWIMMEREVENTCOLLECTION_H
#define COMPETYSPY_FRONT_SWIMMEREVENTCOLLECTION_H

#include <functional>
#include <vector>

#include "Event.h"
#include "Swimmer.h"
#include "SwimmerProxy.h"
#include "Times.h"
#include "sorted_insert.h"

namespace SwimmerEventCollection {

class SwimmerEventProxy {
public:
  SwimmerEventProxy(const Swimmer& swimmer, Event event, Times::Pool pool) :
      swimmer_(swimmer),
      event_(event),
      pool_(pool) {}

  SwimmerEventProxy(SwimmerEventProxy&& swimmer) noexcept :
      swimmer_(swimmer.swimmer_),
      event_(swimmer.event_),
      pool_(swimmer.pool_) {}

  auto operator=(SwimmerEventProxy&& swimmer) -> SwimmerEventProxy& {
    swimmer_ = swimmer.swimmer_;
    return *this;
  }

  [[nodiscard]] auto getId() const -> Swimmer::SwimmerId { return swimmer_.get().getId(); }

  [[nodiscard]] auto getTime() const -> float {
    return swimmer_.get().getTimeAutoConvert(event_, pool_);
  }

  ~SwimmerEventProxy() = default;

  auto operator<=>(const SwimmerEventProxy& that) const { return getTime() <=> that.getTime(); }

private:
  std::reference_wrapper<const Swimmer> swimmer_;
  Event                                 event_;
  Times::Pool                           pool_;
};

using CollectionEntry = std::tuple<Swimmer::SwimmerId, Event, float>;
using Collection      = std::vector<CollectionEntry>;

template<typename SwimmersContainer>
auto createSortedCollection(
    const SwimmersContainer& swimmers, const Event& event, Times::Pool pool,
    const std::function<bool(const Swimmer& sw)>& isSwimmerAvailable
    = [](const Swimmer& swimmer) { return true; }) -> Collection {
  std::vector<SwimmerEventProxy> collection;
  for (const Swimmer& swimmer : swimmers) {
    if (isSwimmerAvailable(swimmer)
        && !StyleTimes::isNoTime(swimmer.getTimeAutoConvert(event, Times::Pool::_25))
        && !StyleTimes::timesEqual(swimmer.getTimeAutoConvert(event, Times::Pool::_25), 0)) {
      insert_sorted(collection, SwimmerEventProxy(swimmer, event, pool));
    }
  }
  Collection r;
  r.reserve(collection.size());
  std::transform(collection.begin(), collection.end(), std::back_inserter(r),
                 [event](const SwimmerEventProxy& s) -> CollectionEntry {
                   CollectionEntry collectionEntry(s.getId(), event, s.getTime());
                   return collectionEntry;
                 });
  return r;
}
};  // namespace SwimmerEventCollection

#endif  // COMPETYSPY_FRONT_SWIMMEREVENTCOLLECTION_H
