#ifndef COMPETYSPY_FRONT_RELAY_H
#define COMPETYSPY_FRONT_RELAY_H

#include <algorithm>
#include <array>
#include <cstddef>
#include <functional>
#include <memory>
#include <vector>

#include "Event.h"
#include "Gender.h"
#include "RelayCategory.h"
#include "RelayObserver.h"
#include "Swimmer.h"
#include "Times.h"

class Relay {
public:
  struct Id {
    Event         event;
    RelayCategory category;
    Times::Pool   pool;

    friend auto   operator<=>(const Id& lhs, const Id& rhs) noexcept = default;
  };

  class RelayNotificationTemporalDisabler {
  public:
    RelayNotificationTemporalDisabler(Relay& relay) :
        relay_(relay),
        isChangeNotificationEnabled_(relay_.isChangeNotificationEnabled()) {
      spdlog::debug("[RelayNotificationTemporalDisabler] disablingNotifications is enabled={}",
                    isChangeNotificationEnabled_);
      relay_.disableObserverNotifications();
    }

    ~RelayNotificationTemporalDisabler() {
      if (isChangeNotificationEnabled_) {
        spdlog::debug("[RelayNotificationTemporalDisabler] enablingNotifications");
        relay_.enableObserverNotifications();
      }
    }

  private:
    Relay& relay_;
    bool   isChangeNotificationEnabled_;
  };

  virtual ~Relay() = default;

  virtual void setSwimmer(std::unique_ptr<Swimmer> swimmer, uint8_t position,
                          const std::function<void(const Relay*)>& callback)
      = 0;

  void setSwimmer(std::unique_ptr<Swimmer> swimmer, uint8_t position) {
    setSwimmer(std::move(swimmer), position, [](const Relay*) {});
  }

  virtual auto               removeSwimmer(uint8_t position) -> std::unique_ptr<Swimmer>   = 0;
  virtual void               clean()                                                       = 0;

  [[nodiscard]] virtual auto isFull() const -> bool                                        = 0;

  [[nodiscard]] virtual auto getId() const -> Id                                           = 0;

  [[nodiscard]] virtual auto getEvent() const -> Event                                     = 0;

  [[nodiscard]] virtual auto getCategory() const -> RelayCategory                          = 0;

  virtual void visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) = 0;
  virtual void visitSwimmers(std::function<void(const Swimmer* swimmer, size_t index)> fn) const
      = 0;

  [[nodiscard]] auto getSumAge(uint16_t year) const -> uint16_t {
    uint16_t age = 0;
    visitSwimmers([&age, year](const Swimmer* swimmer, size_t index) {
      if (swimmer == nullptr) { return; }
      age += swimmer->getAge(year);
    });
    return age;
  }

  [[nodiscard]] auto getPositionData(uint8_t position) const {
    return std::tuple<Event, uint8_t>{getEventForPosition(position), position};
  }

  [[nodiscard]] auto getLeftPositions() const {
    std::vector<std::tuple<Event, uint8_t>> leftPositions;
    leftPositions.reserve(4);
    visitSwimmers([&leftPositions, this](const Swimmer* swimmer, size_t index) {
      if (swimmer == nullptr) { leftPositions.emplace_back(getPositionData(index)); }
    });
    return leftPositions;
  }

  [[nodiscard]] auto getNewSwimmerAgeRange(uint16_t year) const -> std::tuple<uint16_t, uint16_t> {
    const auto     category          = getCategory();
    const uint16_t minAgeForCategory = category == RelayCategory::_80 ? 20 : 25;
    const auto [minSum, maxSum]      = getRelayCategoryRange(category);
    const auto leftPositions         = getLeftPositions();
    const auto relaySumAge           = getSumAge(year);
    return {(leftPositions.size() == 1 && relaySumAge < minSum)
                ? std::max<uint16_t>(minSum - relaySumAge, minAgeForCategory)
                : minAgeForCategory,
            maxSum - relaySumAge - (leftPositions.size() - 1) * minAgeForCategory};
  }

  [[nodiscard]] auto getGenderCount(Gender gender) const -> uint8_t {
    uint8_t count = 0;
    visitSwimmers([&count, gender](const Swimmer* swimmer, size_t index) {
      if (swimmer == nullptr) { return; }
      count += swimmer->getGender() == gender ? 1 : 0;
    });
    return count;
  }

  [[nodiscard]] auto needsSwimmerFromGender(Gender gender) const -> bool {
    switch (getEvent().gender) {
      case EventGender::MALE :
        return gender == Gender::MALE;
      case EventGender::FEMALE :
        return gender == Gender::FEMALE;
      case EventGender::MIXED :
        switch (gender) {
          case Gender::MALE :
            return getGenderCount(Gender::MALE) != 2;
          case Gender::FEMALE :
            return getGenderCount(Gender::FEMALE) != 2;
        }
    }
    return false;
  }

  [[nodiscard]] auto getAccumulatedTime() const -> float {
    float totalTime = 0;
    visitSwimmers([&totalTime, this](const Swimmer* swimmer, size_t index) {
      if (swimmer == nullptr) { return; }
      totalTime += swimmer->getTimeAutoConvert(getEventForPosition(index), getPool());
    });
    return totalTime;
  }

  [[nodiscard]] virtual auto getPool() const -> Times::Pool = 0;

  [[nodiscard]] auto getSwimmerIds() const -> std::array<std::optional<Swimmer::SwimmerId>, 4> {
    std::array<std::optional<Swimmer::SwimmerId>, 4> swimmers{std::nullopt, std::nullopt,
                                                              std::nullopt, std::nullopt};
    visitSwimmers([&swimmers](const Swimmer* swimmer, size_t position) {
      if (nullptr != swimmer) { swimmers[position] = swimmer->getId(); }
    });
    return swimmers;
  }

  auto operator==(Relay const& rhs) const -> bool {
    return (getEvent() == rhs.getEvent()) && (getCategory() == rhs.getCategory());
  }

  struct Hash {
    auto operator()(const Relay& relay) const -> size_t {
      return this->operator()(relay.getEvent(), relay.getCategory());
    }

    auto operator()(const std::tuple<Event, RelayCategory>& key) const -> size_t {
      const auto& [event, category] = key;
      return this->operator()(event, category);
    }

    auto operator()(const Event& event, RelayCategory category) const -> size_t {
      std::size_t h = 0;
      hash_combine(h, Event::Hash()(event), category);
      return h;
    }
  };

  virtual void registerRelayObserver(RelayObserver* relayObserver)   = 0;
  virtual void unregisterRelayObserver(RelayObserver* relayObserver) = 0;

  virtual void disableObserverNotifications() { isChangeNotificationEnabled_ = false; }

  virtual void enableObserverNotifications() { isChangeNotificationEnabled_ = true; }

  [[nodiscard]] virtual auto isChangeNotificationEnabled() const -> bool {
    return isChangeNotificationEnabled_;
  }

protected:
  [[nodiscard]] virtual auto getEventForPosition(size_t position) const -> Event = 0;

private:
  bool isChangeNotificationEnabled_ = true;
};

#endif  // COMPETYSPY_FRONT_RELAY_H
