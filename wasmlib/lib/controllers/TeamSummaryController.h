#ifndef COMPETYSPY_FRONT_GET_TEAM_SESSION_SWIMMER_CONTROLLER_H
#define COMPETYSPY_FRONT_GET_TEAM_SESSION_SWIMMER_CONTROLLER_H

#include <map>
#include <sorted_insert.h>
#include <vector>

#include "spdlog/spdlog.h"

#include "Event.h"
#include "Gender.h"
#include "Swimmer.h"
#include "SwimmerTimesController.h"
#include "TeamOperationsController.h"
#include "Times.h"

class TeamSummaryController {
public:
  struct EventPoolTime {
    Event       event;
    Times::Pool pool = Times::Pool::_25;
    float       time = 0;
  };

  struct SummaryEntry {
    Swimmer::SwimmerId          swimmerId = {"", "", 0, Gender::MALE};
    uint8_t                     age       = 0;
    std::map<std::string, bool> sessionAvailability;
    EventPoolTime               fly50;
    EventPoolTime               fly100;
    EventPoolTime               back50;
    EventPoolTime               back100;
    EventPoolTime               breast50;
    EventPoolTime               breast100;
    EventPoolTime               free50;
    EventPoolTime               free100;
  };

  TeamSummaryController(const Team& team) : team_(team) {}

  static auto getEventPoolTime(const Swimmer& swimmer, const Event& event, Times::Pool pool)
      -> EventPoolTime {
    return {event, pool, swimmer.getTimeAutoConvert(event, pool)};
  }

  auto getSummary(uint16_t year, Times::Pool pool) -> std::vector<SummaryEntry> {
    spdlog::debug("[TeamSummaryController] {}", __FUNCTION__);
    std::vector<SummaryEntry> result;
    const auto&               swimmerIds = getSwimmerIds();
    for (const auto& swimmerId : swimmerIds) {
      SummaryEntry entry;
      entry.swimmerId     = swimmerId;
      const auto& swimmer = team_.getSwimmer(swimmerId);
      entry.age           = year - swimmerId.bornYear;
      const EventGender gender
          = swimmerId.gender == Gender::MALE ? EventGender::MALE : EventGender::FEMALE;
      entry.fly50     = getEventPoolTime(swimmer, {Style::BUTTERFLY, 50, gender}, pool);
      entry.fly100    = getEventPoolTime(swimmer, {Style::BUTTERFLY, 100, gender}, pool);
      entry.back50    = getEventPoolTime(swimmer, {Style::BACK, 50, gender}, pool);
      entry.back100   = getEventPoolTime(swimmer, {Style::BACK, 100, gender}, pool);
      entry.breast50  = getEventPoolTime(swimmer, {Style::BREAST, 50, gender}, pool);
      entry.breast100 = getEventPoolTime(swimmer, {Style::BREAST, 100, gender}, pool);
      entry.free50    = getEventPoolTime(swimmer, {Style::FREE, 50, gender}, pool);
      entry.free100   = getEventPoolTime(swimmer, {Style::FREE, 100, gender}, pool);
      for (const auto& sessionId : team_.getSessionIds()) {
        entry.sessionAvailability.emplace(sessionId,
                                          team_.getSession(sessionId).hasSwimmer(swimmerId));
      }
      insert_sorted<SummaryEntry>(result, std::move(entry),
                                  [](const SummaryEntry& lhs, const SummaryEntry& rhs) {
                                    if (lhs.swimmerId.gender != rhs.swimmerId.gender) {
                                      return lhs.swimmerId.gender == Gender::FEMALE;
                                    }
                                    return lhs.age < rhs.age;
                                  });
    }
    return result;
  }

protected:
  [[nodiscard]] auto getSwimmerIds() const -> std::vector<Swimmer::SwimmerId> {
    std::vector<Swimmer::SwimmerId> swimmers;
    auto                            it = team_.getSwimmerIterator();
    while (it->hasMore()) { swimmers.emplace_back(it->getNext().getId()); }
    return swimmers;
  }

  const Team& team_;
};

#endif
