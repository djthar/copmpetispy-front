#ifndef COMPETYSPY_FRONT_SWIMMERTOSESSIONCONTROLLER_H
#define COMPETYSPY_FRONT_SWIMMERTOSESSIONCONTROLLER_H

#include <utility>

#include "TeamModificationsController.h"

class SwimmerToSessionController {
public:
  SwimmerToSessionController(TeamModificationsController teamModificationsController) :
      teamModificationsController_(std::move(teamModificationsController)) {}

  void addSwimmerToSession(const Swimmer::SwimmerId& swimmerId, const std::string& sessionId) {
    teamModificationsController_.getSessionOperationController(sessionId).addSwimmer(swimmerId);
  }

  void removeSwimmerFromSession(const Swimmer::SwimmerId& swimmerId, const std::string& sessionId) {
    teamModificationsController_.getSessionOperationController(sessionId).removeSwimmer(swimmerId);
  }

  void toggleSwimmerFromSession(const Swimmer::SwimmerId& swimmerId, const std::string& sessionId) {
    auto sessionOperationController
        = teamModificationsController_.getSessionOperationController(sessionId);
    if (sessionOperationController.hasSwimmer(swimmerId)) {
      sessionOperationController.removeSwimmer(swimmerId);
    } else {
      sessionOperationController.addSwimmer(swimmerId);
    }
  }

private:
  TeamModificationsController teamModificationsController_;
};

#endif  // COMPETYSPY_FRONT_SWIMMERTOSESSIONCONTROLLER_H
