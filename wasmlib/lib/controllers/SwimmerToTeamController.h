#ifndef COMPETYSPY_FRONT_SWIMMERTOTEAMCONTROLLER_H
#define COMPETYSPY_FRONT_SWIMMERTOTEAMCONTROLLER_H

#include "SwimmerOperationsController.h"
#include "TeamOperationsController.h"

class SwimmerToTeamController {
public:
  SwimmerToTeamController(SwimmerOperationsController& swimmerOperationsController,
                          TeamOperationsController&    teamOperationsController) :
      swimmerOperationsController_(swimmerOperationsController),
      teamOperationsController_(teamOperationsController) {}

  void addSwimmerToTeam(const Swimmer::SwimmerId& swimmerId, const std::string& teamName) {
    teamOperationsController_.getOrCreateTeam(teamName);
    teamOperationsController_.getTeamModificationController(teamName, defaultYear)
        .addSwimmer(swimmerOperationsController_.getOrCreateSwimmer(swimmerId));
  }

  void removeSwimmerFromTeam(const Swimmer::SwimmerId& swimmerId, const std::string& teamName) {
    teamOperationsController_.getTeamModificationController(teamName, defaultYear)
        .removeSwimmer(static_cast<const SwimmerOperationsController&>(swimmerOperationsController_)
                           .getSwimmer(swimmerId));
  }

private:
  SwimmerOperationsController& swimmerOperationsController_;
  TeamOperationsController&    teamOperationsController_;
  static constexpr uint64_t    defaultYear = 2000;  // Not used for this controller operations
};

#endif  // COMPETYSPY_FRONT_SWIMMERTOTEAMCONTROLLER_H
