#ifndef COMPETYSPY_FRONT_TEAM_OPERATIONS_CONTROLLER_LOCAL_H
#define COMPETYSPY_FRONT_TEAM_OPERATIONS_CONTROLLER_LOCAL_H

#include <SwimmerProvider.h>
#include <TeamLocal.h>
#include <TeamOperationsController.h>
#include <unordered_map>

#include "nlohmann/json.hpp"

class TeamOperationsControllerLocal : public TeamOperationsController {
public:
  virtual ~TeamOperationsControllerLocal() = default;
  TeamOperationsControllerLocal(SwimmerProvider& swimmerProvider);
  TeamOperationsControllerLocal(nlohmann::json& jsonValue, SwimmerProvider& swimmerProvider);
  void               addTeam(const std::string& teamName) override;
  [[nodiscard]] auto getTeam(const std::string& teamName) const -> const Team& override;
  [[nodiscard]] auto hasTeam(const std::string& teamName) const -> bool override;

  operator nlohmann::json() const;

protected:
  auto getModificableTeam(const std::string& teamName) -> Team& override;

private:
  SwimmerProvider&                           swimmerProvider_;
  std::unordered_map<std::string, TeamLocal> teams_;
};

#endif
