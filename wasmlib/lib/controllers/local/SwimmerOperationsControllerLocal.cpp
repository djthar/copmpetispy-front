#include "SwimmerOperationsControllerLocal.h"

#include "nlohmann/json.hpp"

#include "SwimmersCollectionLocal.h"

SwimmerOperationsControllerLocal::SwimmerOperationsControllerLocal(nlohmann::json& jsonValue) :
    swimmers_(jsonValue) {}

void SwimmerOperationsControllerLocal::addSwimmer(const Swimmer::SwimmerId& id) {
  swimmers_.addSwimmer(id);
}

void SwimmerOperationsControllerLocal::removeSwimmer(const Swimmer::SwimmerId& id) {
  swimmers_.removeSwimmer(id);
}

auto SwimmerOperationsControllerLocal::getSwimmer(const Swimmer::SwimmerId& id) const
    -> const Swimmer& {
  return swimmers_.getSwimmer(id);
}

auto SwimmerOperationsControllerLocal::hasSwimmer(const Swimmer::SwimmerId& id) const -> bool {
  return swimmers_.hasSwimmer(id);
}

auto SwimmerOperationsControllerLocal::getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& {
  return swimmers_.getSwimmer(id);
}

auto SwimmerOperationsControllerLocal::getSwimmerIterator() const
    -> std::unique_ptr<ConstSwimmerIterator> {
  return swimmers_.getSwimmerIterator();
}

SwimmerOperationsControllerLocal::operator nlohmann::json() const {
  return static_cast<nlohmann::json>(swimmers_);
}
