#ifndef COMPETYSPY_FRONT_SESSION_SUMMARY_CONTROLLER_H
#define COMPETYSPY_FRONT_SESSION_SUMMARY_CONTROLLER_H

#include <unordered_map>
#include <vector>

#include "Relay.h"
#include "RelaySummaryController.h"
#include "Session.h"
#include "sorted_insert.h"

class SessionSummaryController {
  using RelaySummaryEntry = RelaySummaryController::RelaySummaryEntry;

public:
  SessionSummaryController(const Session& session) : session_(session) {}

  ~SessionSummaryController() { spdlog::debug("SessionSummaryController destroyed"); }

  auto getSummary(uint16_t year) {
    std::vector<RelaySummaryEntry> relays;
    auto                           it = session_.getRelayIterator();
    while (it->hasMore()) {
      const Relay& relay        = it->getNext();
      auto         relaySummary = RelaySummaryController(relay).getSummary(year);
      insert_sorted<RelaySummaryEntry>(
          relays, std::move(relaySummary),
          [](const RelaySummaryEntry& lhs, const RelaySummaryEntry& rhs) {
            return lhs.id < rhs.id;
          });
    }
    return relays;
  }

protected:
  const Session& session_;
};

#endif
