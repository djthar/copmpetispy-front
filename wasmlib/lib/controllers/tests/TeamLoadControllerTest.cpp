#include <gtest/gtest.h>

#include <Event.h>

#include "Gender.h"
#include "RelayHelperControllerLocal.h"
#include "SwimmerTimesController.h"
#include "TeamLoadController.h"
#include "Times.h"

class TeamLoadControllerTest : public ::testing::Test {
public:
  TeamLoadControllerTest() :
      teamLoadController(relayHelperController.getTeamOperationsController(),
                         relayHelperController.getSwimmerOperationsController(),
                         std::string(teamName)) {}

protected:
  RelayHelperControllerLocal        relayHelperController;
  TeamLoadController                teamLoadController;
  static constexpr std::string_view teamName = "team";
};

// TODO
TEST_F(TeamLoadControllerTest,
       GIVEN_a_team_name_WHEN_create_a_TeamLoadController_THEN_team_is_created_with_one_session) {
  EXPECT_TRUE(relayHelperController.getTeamOperationsController().hasTeam(std::string(teamName)));
}

TEST_F(
    TeamLoadControllerTest,
    GIVEN_a_team_load_controller_WHEN_setting_swimmer_time_THEN_swimmer_is_created_in_the_default_session_and_its_time_is_setted) {
  const Swimmer::SwimmerId swimmerId{"sw1", "", 2000, Gender::MALE};
  auto                     swimmerTimesController
      = teamLoadController.getSwimmerTimesControllerForNewSwimmer(swimmerId);
  const Event       event{};
  const float       time = 30;
  const Times::Pool pool = Times::Pool::_25;
  swimmerTimesController.setEventTime(event, time, pool);
  EXPECT_NEAR(static_cast<const SwimmerOperationsController&>(
                  relayHelperController.getSwimmerOperationsController())
                  .getSwimmer(swimmerId)
                  .getTime(event, pool),
              time, 0.0001);
}

TEST_F(
    TeamLoadControllerTest,
    GIVEN_a_team_load_controller_WHEN_creating_a_load_controller_for_same_team_with_a_swimmer_not_in_default_session_THEN_swimmer_is_not_added_to_session) {
  const Swimmer::SwimmerId swimmerId{"sw1", "", 2000, Gender::MALE};
  teamLoadController.getSwimmerTimesControllerForNewSwimmer(swimmerId);
  ASSERT_TRUE(relayHelperController.getTeamOperationsController()
                  .getTeamModificationController(std::string(teamName), 2000)
                  .getSessionOperationController("1")
                  .hasSwimmer(swimmerId));
  relayHelperController.getTeamOperationsController()
      .getTeamModificationController(std::string(teamName), 2000)
      .getSessionOperationController("1")
      .removeSwimmer(swimmerId);

  TeamLoadController newTeamLoadController(relayHelperController.getTeamOperationsController(),
                                           relayHelperController.getSwimmerOperationsController(),
                                           std::string(teamName));
  newTeamLoadController.getSwimmerTimesControllerForNewSwimmer(swimmerId);
  EXPECT_FALSE(relayHelperController.getTeamOperationsController()
                   .getTeamModificationController(std::string(teamName), 2000)
                   .getSessionOperationController("1")
                   .hasSwimmer(swimmerId));
}
