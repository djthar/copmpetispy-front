#include <gtest/gtest.h>

#include <string>

#include "Event.h"
#include "Gender.h"
#include "Relay.h"
#include "RelayCategory.h"
#include "RelayHelperControllerLocal.h"
#include "SessionOperationsController.h"
#include "SessionSummaryController.h"
#include "Style.h"
#include "Swimmer.h"
#include "TeamModificationsController.h"
#include "Times.h"

class SessionSummaryControllerTest : public ::testing::Test {
public:
  SessionSummaryControllerTest() :
      teamModificationsController(
          relayHelperController.getTeamOperationsController().getOrCreateTeamModificationController(
              std::string(teamName), defaultYear)),
      controller([this]() -> const Session& {
        const std::string& sessionName = std::string(sessionId);
        teamModificationsController.addSession(sessionName);
        return teamModificationsController.getSession(sessionName);
      }()),
      sessionController(
          teamModificationsController.getSessionOperationController(std::string(sessionId))) {}

protected:
  RelayHelperControllerLocal        relayHelperController;
  TeamModificationsController       teamModificationsController;
  SessionSummaryController          controller;
  SessionOperationsController       sessionController;
  static constexpr std::string_view teamName           = "team";
  static constexpr std::string_view sessionId          = "1";
  static constexpr uint64_t         defaultYear        = 2020;
  static constexpr Times::Pool      defaultPool        = Times::Pool::_25;
  static constexpr uint16_t         bornYear30yearsOld = defaultYear - 30;
};

// TODO
TEST_F(SessionSummaryControllerTest,
       GIVEN_an_empty_session_WHEN_get_summary_THEN_vector_has_size_0) {
  EXPECT_EQ(controller.getSummary(defaultYear).size(), 0);
}

TEST_F(
    SessionSummaryControllerTest,
    GIVEN_a_session_with_2_relays_2_women_different_category_2_men_different_category_2_mixed_different_category_WHEN_get_summary_THEN_men_are_first_sorted_by_age_then_women_sorted_by_age_then_mixed_sorted_by_age) {
  const Event relayMale{Style::FREE, 50, EventGender::MALE};
  const Event relayFemale{Style::FREE, 50, EventGender::FEMALE};
  const Event relayMixed{Style::FREE, 50, EventGender::MIXED};
  Relay::Id   relayMenOld{relayMale, RelayCategory::_200, defaultPool};
  Relay::Id   relayMenYong{relayMale, RelayCategory::_100, defaultPool};
  Relay::Id   relayFemaleOld{relayFemale, RelayCategory::_200, defaultPool};
  Relay::Id   relayFemaleYong{relayFemale, RelayCategory::_100, defaultPool};
  Relay::Id   relayMixedOld{relayMixed, RelayCategory::_200, defaultPool};
  Relay::Id   relayMixedYong{relayMixed, RelayCategory::_100, defaultPool};
  sessionController.addRelay(relayMixedYong);
  sessionController.addRelay(relayMixedOld);
  sessionController.addRelay(relayMenOld);
  sessionController.addRelay(relayMenYong);
  sessionController.addRelay(relayFemaleOld);
  sessionController.addRelay(relayFemaleYong);
  const auto summary = controller.getSummary(defaultYear);
  ASSERT_EQ(summary.size(), 6);
  EXPECT_EQ(summary[0].id, relayMenYong);
  EXPECT_EQ(summary[1].id, relayMenOld);
  EXPECT_EQ(summary[2].id, relayFemaleYong);
  EXPECT_EQ(summary[3].id, relayFemaleOld);
  EXPECT_EQ(summary[4].id, relayMixedYong);
  EXPECT_EQ(summary[5].id, relayMixedOld);
}

TEST_F(
    SessionSummaryControllerTest,
    GIVEN_a_session_with_1_relay_with_3_swimmers_WHEN_get_summary_THEN_not_filled_entry_is_null_others_have_correct_swimmer) {
  Swimmer::SwimmerId sw1{"sw1", "", bornYear30yearsOld, Gender::MALE};
  Swimmer::SwimmerId sw2{"sw2", "", bornYear30yearsOld, Gender::MALE};
  Swimmer::SwimmerId sw3{"sw3", "", bornYear30yearsOld, Gender::MALE};

  auto&              swimmerController = relayHelperController.getSwimmerOperationsController();
  swimmerController.addSwimmer(sw1);
  swimmerController.addSwimmer(sw2);
  swimmerController.addSwimmer(sw3);

  sessionController.addSwimmer(sw1);
  sessionController.addSwimmer(sw2);
  sessionController.addSwimmer(sw3);

  const Event event{Style::FREE, 50, EventGender::MALE};
  Relay::Id   relayId{event, RelayCategory::_120, defaultPool};
  sessionController.addRelay(relayId);

  sessionController.getRelayOperationsController(relayId).setSwimmer(sw1, 0);
  sessionController.getRelayOperationsController(relayId).setSwimmer(sw2, 2);
  sessionController.getRelayOperationsController(relayId).setSwimmer(sw3, 3);

  const auto summary = controller.getSummary(defaultYear);
  ASSERT_EQ(summary.size(), 1);
  EXPECT_EQ(summary[0].id, relayId);
  ASSERT_TRUE(summary[0].swimmers[0].available());
  EXPECT_EQ(summary[0].swimmers[0].id(), sw1);
  EXPECT_FALSE(summary[0].swimmers[1].available());
  ASSERT_TRUE(summary[0].swimmers[2].available());
  EXPECT_EQ(summary[0].swimmers[2].id(), sw2);
  ASSERT_TRUE(summary[0].swimmers[3].available());
  EXPECT_EQ(summary[0].swimmers[3].id(), sw3);
}
