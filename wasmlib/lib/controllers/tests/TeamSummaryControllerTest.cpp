#include <gtest/gtest.h>

#include "Gender.h"
#include "RelayHelperControllerLocal.h"
#include "Swimmer.h"
#include "TeamSummaryController.h"
#include "Times.h"

class TeamSummaryControllerTest : public ::testing::Test {
public:
  TeamSummaryControllerTest() :
      team(relayHelperController.getTeamOperationsController().getOrCreateTeam(
          std::string(teamName))),
      controller(team) {}

protected:
  RelayHelperControllerLocal        relayHelperController;
  const Team&                       team;
  TeamSummaryController             controller;
  static constexpr std::string_view teamName           = "team";
  static constexpr uint64_t         defaultYear        = 2020;
  static constexpr Times::Pool      defaultPool        = Times::Pool::_25;
  static constexpr uint16_t         bornYear20yearsOld = defaultYear - 20;
  static constexpr uint16_t         bornYear21yearsOld = defaultYear - 21;
};

// TODO
TEST_F(TeamSummaryControllerTest,
       GIVEN_an_empty_team_name_WHEN_get_summary_THEN_vector_has_size_0) {
  EXPECT_EQ(controller.getSummary(defaultYear, defaultPool).size(), 0);
}

TEST_F(
    TeamSummaryControllerTest,
    GIVEN_a_team_with_2_men_2_women_different_age_WHEN_get_summary_THEN_women_are_first_sorted_by_age_then_men_sorted_by_age) {
  Swimmer::SwimmerId sw1{"sw1", "", bornYear21yearsOld, Gender::MALE};
  Swimmer::SwimmerId sw2{"sw2", "", bornYear20yearsOld, Gender::MALE};
  Swimmer::SwimmerId sw3{"sw3", "", bornYear21yearsOld, Gender::FEMALE};
  Swimmer::SwimmerId sw4{"sw4", "", bornYear20yearsOld, Gender::FEMALE};
  auto loadController = relayHelperController.getTeamLoadController(std::string(teamName));
  loadController.getSwimmerTimesControllerForNewSwimmer(sw1);
  loadController.getSwimmerTimesControllerForNewSwimmer(sw2);
  loadController.getSwimmerTimesControllerForNewSwimmer(sw3);
  loadController.getSwimmerTimesControllerForNewSwimmer(sw4);
  const auto summary = controller.getSummary(defaultYear, defaultPool);
  ASSERT_EQ(summary.size(), 4);
  EXPECT_EQ(summary[0].swimmerId, sw4);
  EXPECT_EQ(summary[1].swimmerId, sw3);
  EXPECT_EQ(summary[2].swimmerId, sw2);
  EXPECT_EQ(summary[3].swimmerId, sw1);
}
