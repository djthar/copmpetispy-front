#ifndef COMPETYSPY_FRONT_TEAM_MODIFICATIONS_CONTROLLER_H
#define COMPETYSPY_FRONT_TEAM_MODIFICATIONS_CONTROLLER_H

#include <string>

#include "Session.h"
#include "SessionOperationsController.h"
#include "Swimmer.h"
#include "SwimmerProvider.h"
#include "Team.h"

class TeamModificationsController : public SwimmerProvider {
public:
  TeamModificationsController(Team& team, uint16_t year) : team_(team), year_(year) {}

  void addSession(const std::string& sessionId) {
    if (hasSession(sessionId)) { return; }
    team_.addSession(sessionId);
    Session& session         = team_.getSession(sessionId);
    auto     swimmerIterator = team_.getSwimmerIterator();
    while (swimmerIterator->hasMore()) {
      team_.getSession(sessionId);
      session.addSwimmer(swimmerIterator->getNext().getId());
    }
  }

  void removeSession(const std::string& sessionId) {
    if (hasSession(sessionId)) { team_.removeSession(sessionId); }
  }

  [[nodiscard]] auto hasSession(const std::string& sessionId) const -> bool {
    return team_.hasSession(sessionId);
  }

  void addSwimmer(const Swimmer& swimmer) { addSwimmer(swimmer.getId()); }

  void addSwimmer(const Swimmer::SwimmerId& swimmer) { team_.addSwimmer(swimmer); }

  void removeSwimmer(const Swimmer& swimmer) { removeSwimmer(swimmer.getId()); }

  void removeSwimmer(const Swimmer::SwimmerId& swimmer) { team_.removeSwimmer(swimmer); }

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override {
    return team_.getSwimmer(id);
  }

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override {
    return team_.getSwimmer(id);
  }

  [[nodiscard]] auto getSwimmerIterator() const -> std::unique_ptr<ConstSwimmerIterator> override {
    return team_.getSwimmerIterator();
  }

  [[nodiscard]] auto getSession(const std::string& sessionId) const -> const Session& {
    return team_.getSession(sessionId);
  }

  [[nodiscard]] auto getSessionIds() const -> std::vector<std::string> {
    return team_.getSessionIds();
  }

  auto getSessionOperationController(const std::string& sessionId) -> SessionOperationsController {
    return SessionOperationsController{team_.getSession(sessionId), year_};
  }

private:
  Team&    team_;
  uint16_t year_;
};

#endif
