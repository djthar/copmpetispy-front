#ifndef COMPETYSPY_FRONT_RELAYHELPERCONTROLLER_H
#define COMPETYSPY_FRONT_RELAYHELPERCONTROLLER_H

#include <cstdint>
#include <string>

#include "RelayObserver.h"
#include "RelaySetSwimmerController.h"
#include "SessionSummaryController.h"
#include "SwimmerOperationsController.h"
#include "SwimmerToSessionController.h"
#include "SwimmerToTeamController.h"
#include "TeamLoadController.h"
#include "TeamOperationsController.h"
#include "TeamSummaryController.h"

class RelayHelperController {
public:
  virtual ~RelayHelperController()                                              = default;
  virtual auto getSwimmerOperationsController() -> SwimmerOperationsController& = 0;
  virtual auto getTeamOperationsController() -> TeamOperationsController&       = 0;

  auto         getSwimmerToTeamController() -> SwimmerToTeamController {
    return {getSwimmerOperationsController(), getTeamOperationsController()};
  }

  auto getSwimmerToSessionController(const std::string& teamName) -> SwimmerToSessionController {
    spdlog::debug("getSwimmerToSessionController");
    return {getTeamOperationsController().getTeamModificationController(teamName, defaultYear)};
  }

  auto getTeamLoadController(const std::string& teamName) -> TeamLoadController {
    return {getTeamOperationsController(), getSwimmerOperationsController(), teamName};
  }

  auto getTeamSummaryController(const std::string& teamName) -> TeamSummaryController {
    return TeamSummaryController(getTeamOperationsController().getTeam(teamName));
  }

  auto getSessionsSummaryController(const std::string& teamName, const std::string& sessionId,
                                    uint16_t year) -> SessionSummaryController {
    return SessionSummaryController(getTeamOperationsController()
                                        .getTeamModificationController(std::string(teamName), year)
                                        .getSession(sessionId));
  }

  auto getRelaySetSwimmerController(const std::string& teamName, const std::string& sessionId,
                                    const Relay::Id relayId, uint16_t year, uint8_t position)
      -> RelaySetSwimmerController {
    return {getTeamOperationsController()
                .getTeamModificationController(teamName, year)
                .getSessionOperationController(sessionId)
                .getRelayOperationsController(relayId),
            position};
  }

  auto getAvailableSessionIds(const std::string& teamName) {
    return getTeamOperationsController().getSessionIds(teamName);
  }

  auto addNewSession(const std::string& teamName, const std::string& sessionId) {
    return getTeamOperationsController()
        .getTeamModificationController(teamName, defaultYear)
        .addSession(sessionId);
  }

  auto removeSession(const std::string& teamName, const std::string& sessionId) {
    return getTeamOperationsController()
        .getTeamModificationController(teamName, defaultYear)
        .removeSession(sessionId);
  }

  auto registerRelayChangedObserver(const std::string& teamName, const std::string& sessionId,
                                    const Relay::Id relayId, RelayObserver* relayObserver) {
    getTeamOperationsController()
        .getTeamModificationController(teamName, defaultYear)
        .getSessionOperationController(sessionId)
        .getRelayOperationsController(relayId)
        .registerRelayChangeObserver(relayObserver);
  }

  auto unregisterRelayChangedObserver(const std::string& teamName, const std::string& sessionId,
                                      const Relay::Id relayId, RelayObserver* relayObserver) {
    getTeamOperationsController()
        .getTeamModificationController(teamName, defaultYear)
        .getSessionOperationController(sessionId)
        .getRelayOperationsController(relayId)
        .unregisterRelayChangeObserver(relayObserver);
  }

private:
  static constexpr uint64_t defaultYear = 2000;  // Not used for this controller operations
};

#endif  // COMPETYSPY_FRONT_RELAYHELPERCONTROLLER_H
