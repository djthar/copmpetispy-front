#ifndef COMPETYSPY_FRONT_RELAY_SET_SWIMMER_CONTROLLER_H
#define COMPETYSPY_FRONT_RELAY_SET_SWIMMER_CONTROLLER_H

#include <cstdint>
#include <utility>

#include "RelayOperationsController.h"
#include "Swimmer.h"

class RelaySetSwimmerController {
public:
  RelaySetSwimmerController(RelayOperationsController relayOperationsController, uint8_t position) :
      relayOperationsController_(std::move(relayOperationsController)),
      position_(position) {}

  [[nodiscard]] auto getAvailableSwimmers() const {
    return relayOperationsController_.getSwimmersForPosition(position_);
  }

  void setSwimmer(const Swimmer::SwimmerId& swimmerId) {
    relayOperationsController_.setSwimmer(swimmerId, position_);
  }

  void removeSwimmer() { relayOperationsController_.removeSwimmer(position_); }

private:
  RelayOperationsController relayOperationsController_;
  uint8_t                   position_;
};

#endif
