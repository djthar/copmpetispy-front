#ifndef COMPETYSPY_FRONT_SWIMMER_OPERATIONS_CONTROLLER_H
#define COMPETYSPY_FRONT_SWIMMER_OPERATIONS_CONTROLLER_H

#include <utility>

#include "Swimmer.h"
#include "SwimmerProvider.h"
#include "SwimmerTimesController.h"

class SwimmerOperationsController : public SwimmerProvider {
public:
  virtual void addSwimmer(const Swimmer::SwimmerId& id) = 0;

  void         addSwimmer(std::string name, std::string surname, uint16_t bornYear, Gender gender) {
    addSwimmer({std::move(name), std::move(surname), bornYear, gender});
  }

  virtual void removeSwimmer(const Swimmer::SwimmerId& id) = 0;

  void removeSwimmer(std::string name, std::string surname, uint16_t bornYear, Gender gender) {
    removeSwimmer({std::move(name), std::move(surname), bornYear, gender});
  }

  void               removeSwimmer(const Swimmer& swimmer) { removeSwimmer(swimmer.getId()); }

  [[nodiscard]] auto getSwimmer(const Swimmer::SwimmerId& id) const -> const Swimmer& override = 0;

  [[nodiscard]] auto getSwimmer(std::string name, std::string surname, uint16_t bornYear,
                                Gender gender) const -> const Swimmer& {
    return getSwimmer({std::move(name), std::move(surname), bornYear, gender});
  }

  [[nodiscard]] virtual auto hasSwimmer(const Swimmer::SwimmerId& id) const -> bool = 0;

  [[nodiscard]] auto         hasSwimmer(std::string name, std::string surname, uint16_t bornYear,
                                        Gender gender) const -> bool {
    return hasSwimmer({std::move(name), std::move(surname), bornYear, gender});
  }

  auto getOrCreateSwimmer(const Swimmer::SwimmerId& id) -> const Swimmer& {
    if (not hasSwimmer(id)) { addSwimmer(id); }
    return getSwimmer(id);
  }

  auto getOrCreateSwimmer(std::string name, std::string surname, uint16_t bornYear,
                          Gender gender) -> const Swimmer& {
    return getOrCreateSwimmer({std::move(name), std::move(surname), bornYear, gender});
  }

  auto getSwimmerTimesController(const Swimmer::SwimmerId& id) -> SwimmerTimesController {
    return SwimmerTimesController{getSwimmer(id)};
  }

  auto getSwimmerTimesController(std::string name, std::string surname, uint16_t bornYear,
                                 Gender gender) -> SwimmerTimesController {
    return getSwimmerTimesController({std::move(name), std::move(surname), bornYear, gender});
  }

  auto getSwimmerTimesController(const Swimmer& swimmer) -> SwimmerTimesController {
    return getSwimmerTimesController(swimmer.getId());
  }

protected:
  auto getSwimmer(const Swimmer::SwimmerId& id) -> Swimmer& override = 0;
};

#endif
