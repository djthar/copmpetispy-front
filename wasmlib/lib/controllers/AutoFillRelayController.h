#ifndef COMPETYSPY_FRONT_AUTOFILLRELAYCONTROLLER_H
#define COMPETYSPY_FRONT_AUTOFILLRELAYCONTROLLER_H

#include <optional>

#include "Relay.h"
#include "Session.h"
#include "Swimmer.h"

class AutoFillRelayController {
public:
  AutoFillRelayController(Relay& relay, Session& session, uint16_t actualYear) :
      relay_(relay),
      session_(session),
      actualYear_(actualYear) {}

  virtual ~AutoFillRelayController() = default;

  void fill() {
    const bool relayNotificationsEnabled = relay_.isChangeNotificationEnabled();
    relay_.disableObserverNotifications();
    autoFindBestRelay();
    if (!StyleTimes::isNoTime(bestTime_)) {
      const auto& missing = relay_.getLeftPositions();
      for (const auto& missingPosition : missing) {
        auto [_, position]                = missingPosition;
        const auto bestSwimmerForPosition = bestRelay_[position];
        if (not bestSwimmerForPosition) { continue; }
        session_.setSwimmerInRelayPosition(*bestSwimmerForPosition, relay_.getId(), position,
                                           [relayNotificationsEnabled, this](const Relay* relay) {
                                             if (relayNotificationsEnabled) {
                                               relay_.enableObserverNotifications();
                                             }
                                           });
      }
    }
    if (relayNotificationsEnabled) { relay_.enableObserverNotifications(); }
  }

  [[nodiscard]] auto getSwimmersForPosition(size_t position) const {
    Relay::RelayNotificationTemporalDisabler relayNotificationTemporalDisabler(relay_);
    auto                                     actualSwimmer = relay_.removeSwimmer(position);
    auto [event, _]                                        = relay_.getPositionData(position);
    auto [minAge, maxAge]    = relay_.getNewSwimmerAgeRange(actualYear_);
    auto isSwimmerSelectable = [this, minAge, maxAge, &actualSwimmer](const Swimmer& sw) {
      const auto swimmerAge = sw.getAge(actualYear_);
      return (actualSwimmer != nullptr && sw == *actualSwimmer)
          || (swimmerAge >= minAge && swimmerAge <= maxAge
              && relay_.needsSwimmerFromGender(sw.getGender()));
    };
    std::optional<const Swimmer::SwimmerId> actualSwimmerId
        = actualSwimmer != nullptr ? std::make_optional(actualSwimmer->getId()) : std::nullopt;
    actualSwimmer = nullptr;
    auto swimmersForPosition
        = session_.getAvailableSwimmersForEvent(event, relay_.getPool(), isSwimmerSelectable);
    if (actualSwimmerId.has_value()) {
      session_.setSwimmerInRelayPosition(*actualSwimmerId, relay_.getId(), position);
    }
    return swimmersForPosition;
  }

protected:
  void autoFindBestRelay() {
    const auto& missing = relay_.getLeftPositions();
    if (missing.empty()) { return; }
    auto [_, position] = missing[0];
    for (const auto& [swimmerId, event, time] : getSwimmersForPosition(position)) {
      session_.setSwimmerInRelayPosition(
          swimmerId, relay_.getId(), position,
          [this](const Relay* relay) { this->relayCompleted(relay); });
      autoFindBestRelay();
      relay_.removeSwimmer(position);
    }
  }

  void relayCompleted(const Relay* relay) {
    const auto time = relay->getAccumulatedTime();
    if (!StyleTimes::timesEqual(time, bestTime_) && time < bestTime_) {
      bestTime_  = time;
      bestRelay_ = relay_.getSwimmerIds();
    }
  }

  Relay&                                           relay_;
  Session&                                         session_;
  const uint16_t                                   actualYear_;
  float                                            bestTime_ = StyleTimes::NO_TIME;
  std::array<std::optional<Swimmer::SwimmerId>, 4> bestRelay_{std::nullopt, std::nullopt,
                                                              std::nullopt, std::nullopt};
};

#endif  // COMPETYSPY_FRONT_AUTOFILLRELAYCONTROLLER_H
