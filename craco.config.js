module.exports = {
  reactScriptsVersion: "wasm-react-scripts",
  webpack: {
    configure: (webpackConfig, { env, paths }) => {
      webpackConfig.module.rules = [
        {
          test: /\.(wasm)$/,
          type: "javascript/auto",
          use: [
            {
              loader: require.resolve("file-loader"),
            },
          ],
        },
        ...webpackConfig.module.rules,
      ];
      return webpackConfig;
    },
  },
};

