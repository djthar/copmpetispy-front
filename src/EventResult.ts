import { distancesT, seassonsMap, stylesT } from "./values";

export type EventResult = {
  _id?: string;
  date: string;
  event: {
    chronometer_type: "E" | "M";
    pool_length: 25 | 50;
    distance: distancesT;
    style: stylesT;
  };
  participant: {
    born_year: number;
    surname: string;
    name: string;
  };
  championship?: string;
  season: keyof seassonsMap<void>;
  time: number;
  when?: string;
};
