import { EventResult } from "./EventResult";
import { distancesT, seassonsMap, stylesT } from "./values";

export interface EventResultController {
  get_best_event_result: (
    distance: distancesT,
    style: stylesT,
    pool: 25 | 50,
    chronometer_type: "E" | "M" | null,
    season: keyof seassonsMap<void>,
    setEventResult: (eventResult: EventResult | undefined) => void
  ) => void;
  get_event_results: (
    distance: distancesT,
    style: stylesT,
    pool: 25 | 50,
    setEventResult: (eventResult: EventResult | undefined) => void,
    chronometer_type: "E" | "M" | null,
    result_size: number,
    season: keyof seassonsMap<void>,
    sort_mode: string,
    sort_direction: 0 | 1,
    results_limit: number,
    start_index: number
  ) => void;
  get_unique_id: () => string;
}

export type EventResultControllerBuilder = (
  name: string,
  surname: string,
  born_year: number
) => EventResultController;
